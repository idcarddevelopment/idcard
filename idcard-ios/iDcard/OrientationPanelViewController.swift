//
//  OrientationPanelViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/2/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class OrientationPanelViewController: UIViewController {
	
	var orientationViewController:OrientationViewController?
	
	@IBOutlet weak var textView: UITextView!
	@IBOutlet weak var textViewHeight: NSLayoutConstraint!
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
	
	override func viewWillLayoutSubviews() {
		if (textView != nil && textViewHeight != nil) {
			textViewHeight.constant = textView.sizeThatFits(CGSizeMake(textView.frame.size.width, CGFloat(MAXFLOAT))).height
		}
	}

	@IBAction func dismiss(sender: AnyObject) {
		
		//mark the app as having been launched already so the user won't see orientation again
		var defaults = NSUserDefaults.standardUserDefaults()
		defaults.setBool(false, forKey: "firstLaunch")
		defaults.synchronize()
		
		orientationViewController!.dismissViewControllerAnimated(true, completion: nil)
	}
}
