//
//  FBLoginDelegate.swift
//  iDcard
//
//  Created by Philip Kirkham on 11/19/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

extension AppDelegate {
    
    func facebookSessionDidOpen() {
        if (FBSession.activeSession().isOpen) {
            FBRequestConnection.startForMeWithCompletionHandler({ (connection: FBRequestConnection!, fbUserData: AnyObject!, error: NSError!) in
                if (error == nil) {
                    var fbEmail = fbUserData["email"] as! String?
					var fbId = fbUserData["id"] as! String?
					var fbName = fbUserData["name"] as! String?
                    if (fbEmail != nil && fbEmail != "" && fbId != nil && fbId != "") {
                        var appDelegate = UIApplication.sharedApplication().delegate! as! AppDelegate
                        var params = ["user[email]": fbEmail!, "user[facebook_id]": fbId!, "user[name]": fbName!]
                        self.cmsConnection!.loginFacebook(params, sender: appDelegate)
                    }
                }
            })
        }
    }
}
