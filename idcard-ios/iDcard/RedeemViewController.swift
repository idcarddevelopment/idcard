//
//  RedeemViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/31/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class RedeemViewController: UIViewController, UITextFieldDelegate {
	
	var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
	var reward = Reward()

	@IBOutlet weak var nameField: UITextField!
	@IBOutlet weak var emailField: UITextField!
	@IBOutlet weak var phoneField: UITextField!

	@IBOutlet weak var waitingView: UIView!

	override func viewDidLoad() {
        super.viewDidLoad()

		applyStyles(false)
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	override func viewWillAppear(animated: Bool) {
		nameField.text = appDelegate.cmsConnection!.name
		emailField.text = appDelegate.cmsConnection!.username
		nameField.becomeFirstResponder()
	}
    
	@IBAction func dismiss(sender: AnyObject) {
		nameField.resignFirstResponder()
		emailField.resignFirstResponder()
		phoneField.resignFirstResponder()
		self.dismissViewControllerAnimated(true, completion: nil)
	}
	
	@IBAction func redeem(sender: AnyObject) {
		performRedeem()
	}
	/*ajith error
	func performRedeem() {
		if (count(nameField.text) < 2) {
			UIAlertView(title: "Form Error", message: "You must enter a valid name", delegate: nil, cancelButtonTitle: "OK").show()
		}
		else if (count(emailField.text) < 5) {
			UIAlertView(title: "Form Error", message: "You must enter a valid email address", delegate: nil, cancelButtonTitle: "OK").show()
		}
		else if (count(phoneField.text) < 14) {
			UIAlertView(title: "Form Error", message: "You must enter a valid phone number", delegate: nil, cancelButtonTitle: "OK").show()
		}
		else {
			waitingView.hidden = false
			appDelegate.cmsConnection!.redeem(self.reward, params: ["name": nameField.text, "email": emailField.text, "phone": phoneField.text], sender: self)
		}
	}
*/
    
    func performRedeem() {
        if ((nameField.text?.characters.count) < 2) {
            UIAlertView(title: "Form Error", message: "You must enter a valid name", delegate: nil, cancelButtonTitle: "OK").show()
        }
        else if ((emailField.text?.characters.count) < 5) {
            UIAlertView(title: "Form Error", message: "You must enter a valid email address", delegate: nil, cancelButtonTitle: "OK").show()
        }
        else if ((phoneField.text?.characters.count) < 14) {
            UIAlertView(title: "Form Error", message: "You must enter a valid phone number", delegate: nil, cancelButtonTitle: "OK").show()
        }
        else {
            waitingView.hidden = false
            appDelegate.cmsConnection!.redeem(self.reward, params: ["name": nameField.text!, "email": emailField.text!, "phone": phoneField.text!], sender: self)
        }
    }
	
	func didRedeem(status: Bool, message: String) {
		waitingView.hidden = true
		
		if (status) {
			appDelegate.rewardsHaveChanged = true
			self.dismissViewControllerAnimated(true, completion: nil)
			UIAlertView(title: "Thank you", message: "We will contact you shortly with details about claiming your prize", delegate: nil, cancelButtonTitle: "OK").show()
		}
		else {
			UIAlertView(title: "Error".localized, message: message, delegate: nil, cancelButtonTitle: "OK").show()
		}
	}
	
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
	
	func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
		let totalString = "\(textField.text)\(string)"
		if (textField == phoneField) {
			if (range.length == 1) {
				textField.text = formatPhoneNumber(totalString, deleteLastChar: true)
			}
			else {
				textField.text = formatPhoneNumber(totalString, deleteLastChar: false)
			}
			return false
		}
		return true
	}
	
//	- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//	NSString* totalString = [NSString stringWithFormat:@"%@%@",textField.text,string];
//	
//	// if it's the phone number textfield format it.
//	if(textField.tag==102 ) {
//	if (range.length == 1) {
//	// Delete button was hit.. so tell the method to delete the last char.
//	textField.text = [self formatPhoneNumber:totalString deleteLastChar:YES];
//	} else {
//	textField.text = [self formatPhoneNumber:totalString deleteLastChar:NO ];
//	}
//	return false;
//	}
//	
//	return YES;
//	}
//
	
	func formatPhoneNumber(number: String, deleteLastChar: Bool) -> String {
		var simpleNumber = number as NSString
		
		if (simpleNumber.length == 0) {
			return ""
		}
		
		/*ajith error 

var regex = NSRegularExpression(pattern: "[\\s-\\(\\)]", options: NSRegularExpressionOptions.CaseInsensitive, error: nil)
        
        simpleNumber = regex!.stringByReplacingMatchesInString(simpleNumber as String, options: NSMatchingOptions.allZeros, range: NSMakeRange(0, simpleNumber.length), withTemplate: "")

*/
        
        var regex = try! NSRegularExpression(pattern: "[\\s-\\(\\)]",
            options: [.CaseInsensitive])

        
		simpleNumber = regex.stringByReplacingMatchesInString(simpleNumber as String, options: NSMatchingOptions(), range: NSMakeRange(0, simpleNumber.length), withTemplate: "")
		if (simpleNumber.length > 10) {
			simpleNumber = simpleNumber.substringToIndex(10)
		}
		
		if (deleteLastChar) {
			simpleNumber = simpleNumber.substringToIndex(simpleNumber.length - 1)
		}
		
//		if (simpleNumber.length < 7) {
//			simpleNumber = simpleNumber.stringByReplacingOccurrencesOfString("(\\d{3})(\\d+)", withString: "($1) $2", options: .RegularExpressionSearch, range: NSMakeRange(0, simpleNumber.length))
//		}
//		else {
			simpleNumber = simpleNumber.stringByReplacingOccurrencesOfString("(\\d{3})(\\d{3})(\\d+)", withString: "($1) $2-$3", options: .RegularExpressionSearch, range: NSMakeRange(0, simpleNumber.length))
//		}
		
		return simpleNumber as String

	}

//	-(NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar {
//	if(simpleNumber.length==0) return @"";
//	// use regex to remove non-digits(including spaces) so we are left with just the numbers
//	NSError *error = NULL;
// NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
// simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:@""];
//	
//	// check if the number is to long
//	if(simpleNumber.length>10) {
//	// remove last extra chars.
//	simpleNumber = [simpleNumber substringToIndex:10];
//	}
//	
//	if(deleteLastChar) {
//	// should we delete the last digit?
//	simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
//	}
//	
//	// 123 456 7890
//	// format the number.. if it's less then 7 digits.. then use this regex.
//	if(simpleNumber.length<7)
//	simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d+)"
//	withString:@"($1) $2"
//	options:NSRegularExpressionSearch
//	range:NSMakeRange(0, [simpleNumber length])];
//	
//	else   // else do this one..
//	simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d+)"
//	withString:@"($1) $2-$3"
//	options:NSRegularExpressionSearch
//	range:NSMakeRange(0, [simpleNumber length])];
//	return simpleNumber;
//	}

}
