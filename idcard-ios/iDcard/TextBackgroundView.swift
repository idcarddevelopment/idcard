//
//  TextBackgroundView.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/30/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class TextBackgroundView: UIView {
	
	var backgroundImage = UIImage(named: "text-box-background")!.resizableImageWithCapInsets(UIEdgeInsetsMake(12.0, 12.0, 12.0, 12.0))

	override func drawRect(rect: CGRect) {
		backgroundImage.drawInRect(self.bounds)
	}

}
