//
//  FacebookAdditions.h
//  iDcard
//
//  Created by Philip Kirkham on 12/4/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <FacebookSDK/FacebookSDK.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>

@protocol FacebookCheckInController
@required
- (void)didPublishFacebookStatus:(BOOL)success;
- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results;
@optional
- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error;
- (void)sharerDidCancel:(id<FBSDKSharing>)sharer;

@end


@interface FacebookAdditions : NSObject

+ (void)checkIn:(NSString *)aType
          title:(NSString *)aTitle
          image:(NSString *)anImage
            url:(NSString *)aUrl
    description:(NSString *)aDescription
checkInViewController:(UIViewController<FacebookCheckInController> *)aCheckInViewController;

+ (void)photoCheckIn:(NSString *)aType
               title:(NSString *)aTitle
               image:(NSString *)anImage
                 url:(NSString *)aUrl
         description:(NSString *)aDescription
               photo:(UIImage *)aPhoto
checkInViewController:(UIViewController<FacebookCheckInController> *)aCheckInViewController;

@end

