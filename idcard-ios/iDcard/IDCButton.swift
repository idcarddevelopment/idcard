//
//  IDCButton.swift
//  iDcard
//
//  Created by Philip Kirkham on 9/25/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class IDCButton: UIButton {
	
	override var enabled: Bool {
		didSet {
			self.layer.opacity = enabled ? 1.0 : 0.5
		}
	}
	
	override var selected: Bool {
		didSet {
			if (selected) {
				self.layer.backgroundColor = UIColor.highlightColor().CGColor
			}
			else {
				self.layer.backgroundColor = UIColor.blackColor().CGColor
			}
		}
	}
	
	override var highlighted: Bool {
		didSet {
			if (highlighted) {
				self.layer.backgroundColor = UIColor.highlightColor().CGColor
			}
			else {
				self.layer.backgroundColor = UIColor.blackColor().CGColor
			}
		}
	}
	//nri error
	//required init(coder aDecoder: NSCoder)
    required init?(coder aDecoder: NSCoder){
		super.init(coder: aDecoder)
		self.layer.borderWidth = 2.0;
		self.layer.borderColor = UIColor.highlightColor().CGColor
		self.adjustsImageWhenHighlighted = false
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		self.layer.borderWidth = 2.0;
		self.layer.borderColor = UIColor.highlightColor().CGColor
		self.adjustsImageWhenHighlighted = false
	}
	
	override func layoutSubviews() {
		self.layer.cornerRadius = min(self.bounds.size.width, self.bounds.size.height)/2.0
		super.layoutSubviews()
		
		//make sure the right background color is drawn initially
		self.highlighted = self.highlighted.boolValue
		self.selected = self.selected.boolValue
		self.enabled = self.enabled.boolValue

		//Allow multiline titles
		self.titleLabel!.textAlignment = NSTextAlignment.Center
		//[button setTitle: @"Line1\nLine2" forState: UIControlStateNormal];

	}

}
