//
//  FavoritesViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 9/24/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class FavoritesViewController: UITableViewController, CMSConnectionDelegate, CMSFavoritesDelegate {
	
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
	
	var locations: [Location] = []
	
	var locationToDelete: Location?
	var indexPathToDelete: NSIndexPath?
	
	var appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		// Uncomment the following line to preserve selection between presentations
		// self.clearsSelectionOnViewWillAppear = false
		
		// Uncomment the following line to display an Edit button in the navigation bar for this view controller.
		// self.navigationItem.rightBarButtonItem = self.editButtonItem()
		
		self.refreshControl = UIRefreshControl()
		self.refreshControl!.addTarget(self, action: Selector("updateLocations"), forControlEvents: UIControlEvents.ValueChanged)
		applyStyles(true)
		
		self.tableView.allowsMultipleSelectionDuringEditing = false
		self.navigationItem.rightBarButtonItem = self.editButtonItem()
	}
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		
		if (!appDelegate.cmsConnection!.loggedIn) {
			self.locations = []
		}
	}
	
	override func viewDidAppear(animated: Bool) {
		super.viewDidAppear(animated)
		if (appDelegate.favoritesHaveChanged || locations.count == 0) {
			self.refreshControl!.beginRefreshing()
			updateLocations()
			appDelegate.favoritesHaveChanged = false
		}
	}
	
	// MARK: - Table view data source
	
	override func numberOfSectionsInTableView(tableView: UITableView?) -> Int {
		if (self.locations.count > 0) {
//			tableView!.separatorStyle = UITableViewCellSeparatorStyle.SingleLine;
			self.tableView.backgroundView = nil;
			return 1;
		}
		else {
			if (appDelegate.cmsConnection!.loggedIn) {
				var imageView = UIImageView(image: UIImage(named: "favorites-placeholder"))
				imageView.frame = CGRectMake(0.0, 0.0, self.view.bounds.width, self.view.bounds.height)
				imageView.contentMode = UIViewContentMode.Center
				
				self.tableView.backgroundView = imageView;
			}
			else {
				var notLoggedInLabel = UILabel(frame: CGRectMake(40.0, 0.0, self.view.bounds.width-80.0, self.view.bounds.height))
				notLoggedInLabel.textColor = UIColor.whiteColor()
				notLoggedInLabel.text = "You must sign in to save your favorite locations".localized
				notLoggedInLabel.textAlignment = NSTextAlignment.Center
				notLoggedInLabel.numberOfLines = 2
				self.tableView.backgroundView = notLoggedInLabel;
			}
//			self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
			
		}
		
		return 1;
	}
	
	override func tableView(tableView: UITableView?, numberOfRowsInSection section: Int) -> Int {
		return self.locations.count
	}
	
	override func tableView(tableView: UITableView?, cellForRowAtIndexPath indexPath: NSIndexPath?) -> UITableViewCell {
		let cell = tableView!.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath!) as! UITableViewCell
		
		var imageView = cell.viewWithTag(100) as! UIImageView
		var titleLabel = cell.viewWithTag(101) as! UILabel
		var distanceLabel = cell.viewWithTag(102) as! UILabel
		var categoriesLabel = cell.viewWithTag(103) as! UILabel
		
		var location = self.locations[indexPath!.row] as Location
		if appDelegate.currentLocation != nil {
			location.distance = self.appDelegate.currentLocation!.distanceFromLocation(location.clLocation()) * 0.000621371 // meters to miles
		}
		
		titleLabel.text = location.name
		titleLabel.sizeToFit()
		
		if (location.logo != nil) {
			imageView.image = location.logo!
		}
		else if (location.logo_url != "") {
			imageView.image = nil
			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
				var newLogo = UIImage(data: NSData(contentsOfURL: NSURL(string: location.logo_url)!)!)
				dispatch_async(dispatch_get_main_queue()) {
					location.logo = newLogo
					var visibleCell = tableView!.cellForRowAtIndexPath(indexPath!)
					if (visibleCell != nil) {
						imageView.image = newLogo
					}
				}
			}
		}
		imageView.layer.minificationFilter = kCAFilterTrilinear
		
		if appDelegate.currentLocation == nil {
			distanceLabel.text = ""
		}
		else {
			distanceLabel.text = location.formattedDistance()
		}
		categoriesLabel.text = location.categories.combine(", ")
		
		var regularAttributes = [NSFontAttributeName : UIFont.systemFontOfSize(14.0)]
		var boldAttributes = [NSFontAttributeName : UIFont.boldSystemFontOfSize(14.0)]
		
		var selectedView = UIView()
		selectedView.backgroundColor = UIColor.highlightColor()
		cell.selectedBackgroundView = selectedView
		
		return cell
	}
	
	override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
		return true
	}
	
	override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
		if (editingStyle == UITableViewCellEditingStyle.Delete) {
			locationToDelete = self.locations[indexPath.row] as Location
			indexPathToDelete = indexPath
			appDelegate.cmsConnection!.deleteFavorite(locationToDelete!, sender: self)
		}
	}
	
	/*
	// Override to support conditional editing of the table view.
	override func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
	// Return NO if you do not want the specified item to be editable.
	return true
	}
	*/
	
	/*
	// Override to support editing the table view.
	override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
	if editingStyle == .Delete {
	// Delete the row from the data source
	tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
	} else if editingStyle == .Insert {
	// Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
	}
	}
	*/
	
	/*
	// Override to support rearranging the table view.
	override func tableView(tableView: UITableView!, moveRowAtIndexPath fromIndexPath: NSIndexPath!, toIndexPath: NSIndexPath!) {
	
	}
	*/
	
	/*
	// Override to support conditional rearranging of the table view.
	override func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
	// Return NO if you do not want the item to be re-orderable.
	return true
	}
	*/
	
	
	// MARK: - Navigation
	
	// In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
		var detailViewController = segue.destinationViewController as! LocationDetailViewController
		
        //nri error
        //var selectedPath = self.tableView.indexPathForSelectedRow()!
        var selectedPath = self.tableView.indexPathForSelectedRow!
		detailViewController.location = self.locations[selectedPath.row]
	}
	
	
	// MARK: - CMSConnectionDelegate
	
	func setCMSError(error: NSError) {
		//UIAlertView(title: "Error", message: error.localizedDescription, delegate: self, cancelButtonTitle: "OK").show()
        self.performSegueWithIdentifier("login", sender: self)
	}
	
	func updateLocations() {
		if (appDelegate.cmsConnection!.loggedIn) {
			loadingIndicator.startAnimating()
			appDelegate.cmsConnection!.getFavorites(self)
		}
		else {
            loadingIndicator.stopAnimating()
			self.refreshControl!.endRefreshing()
		}
	}
	
	func setAllLocations(_locations: Array<Location>) {
        loadingIndicator.stopAnimating()
		self.locations = _locations
		self.tableView.reloadData()
		self.refreshControl?.endRefreshing()
	}
	
	
	// Mark: - CMSFavoritesDelegate
	
	func didSetFavorite(_status: Bool) {
	}
	
	func didDeleteFavorite(_status: Bool) {
		if (_status && indexPathToDelete != nil) {
			locations.removeAtIndex(indexPathToDelete!.row)
			self.tableView.deleteRowsAtIndexPaths([indexPathToDelete!], withRowAnimation: UITableViewRowAnimation.Automatic)
		}
		else {
			self.tableView.reloadData()
			UIAlertView(title: "Server error", message: "Sorry, an unexpected error occurred and the favorite could not be deleted.  Please try again.", delegate: self, cancelButtonTitle: "OK").show()
		}
		locationToDelete = nil
		indexPathToDelete = nil
	}
	
}
