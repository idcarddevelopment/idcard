//
//  CMSConnection.swift
//  iDcard
//
//  Created by Philip Kirkham on 9/18/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class CMSConnection: NSObject, NSURLConnectionDelegate, NSURLConnectionDataDelegate {
	
//	let SITE = "http://app.theidcard.com"
    let SITE = "http://staging.theidcard.com" //for testing localization
	//let SITE = "http://big-mac.local:3000"
	var LOGIN_URL: NSString
	var LOGOUT_URL: NSString
	var LOCATIONS_URL: NSString
	let manager = AFHTTPRequestOperationManager()
	var delegate: CMSConnectionDelegate?
	
	var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
	
	var responseData: NSMutableData?
	
	var username: String?
	var password: String?
	var facebook_id: String?
	
	var name: String?
	
	var loggedIn: Bool = false
	
	
	init(username aUsername: String, password aPassword: String) {
		username = aUsername
		password = aPassword
		
		LOGIN_URL = "\(SITE)/users/sign_in.json"
		LOGOUT_URL = "\(SITE)/users/sign_out.json"
		LOCATIONS_URL = "\(SITE)/api/locations.json"
		
		super.init()
		
	}
	
	func login(sender: CMSLoginDelegate!) {
		if (username != nil && password != nil) {
			manager.POST(
				LOGIN_URL as String,
				parameters: ["user[email]" : username!, "user[password]" : password!],
				success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
					self.loggedIn = responseObject.valueForKey("success") as! Bool
					if (self.loggedIn) {
						self.name = responseObject.valueForKey("name") as? String
						var defaults = NSUserDefaults.standardUserDefaults()
						defaults.setValue(self.username!, forKey: "username")
						defaults.setValue(self.password!, forKey: "password")
						defaults.synchronize()
						
						sender.loginSucceeded()
					}
					else {
						sender.loginFailed()
					}
				},
				failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
					print("Error: " + error.localizedDescription)
					sender.loginFailed()
				}
			)
		}
		appDelegate.rewardsHaveChanged = true
	}
	
	func loginFacebook(params: Dictionary<String,String>, sender: CMSLoginDelegate!) {
		let FACEBOOK_LOGIN_URL = "\(SITE)/users/facebook_sign_in.json"
			manager.POST(
				FACEBOOK_LOGIN_URL,
				parameters: params,
				success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
					self.loggedIn = responseObject.valueForKey("success") as! Bool
					if (self.loggedIn) {
						self.name = responseObject.valueForKey("name") as? String
						self.username = params["user[email]"]
						self.facebook_id = params["user[facebook_id]"]
						
						var defaults = NSUserDefaults.standardUserDefaults()
						defaults.setValue(self.username!, forKey: "username")
						defaults.setValue(self.facebook_id!, forKey: "facebook_id")
						defaults.synchronize()
						
						sender.loginSucceeded()
					}
					else {
						sender.loginFailed()
					}
				},
				failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
					print("Error: " + error.localizedDescription)
					sender.loginFailed()
				}
			)
		appDelegate.rewardsHaveChanged = true
	}
	
	func logout(sender: AnyObject!) {
		var cookies = NSHTTPCookieStorage.sharedHTTPCookieStorage().cookiesForURL(NSURL(string: SITE)!)
		for cookie in cookies! {
			NSHTTPCookieStorage.sharedHTTPCookieStorage().deleteCookie(cookie as! NSHTTPCookie)
		}
		self.loggedIn = false
		
		var defaults = NSUserDefaults.standardUserDefaults()
		defaults.setValue(nil, forKey: "username")
		defaults.setValue(nil, forKey: "password")
        defaults.setValue(nil, forKey: "facebook_id")
		defaults.synchronize()
        
        if (FBSession.activeSession().isOpen) {
            FBSession.activeSession().closeAndClearTokenInformation();
        }

		appDelegate.rewardsHaveChanged = true


		//		if (loggedIn) {
		//			manager.GET(
		//				LOGOUT_URL,
		//				parameters: [],
		//				success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
		//					self.loggedIn = !(responseObject.valueForKey("success") as Bool)
		//
		//					var defaults = NSUserDefaults.standardUserDefaults()
		//					defaults.setValue(nil, forKey: "username")
		//					defaults.setValue(nil, forKey: "password")
		//					defaults.synchronize()
		//				},
		//				failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
		//					println("Error: " + error.localizedDescription)
		//				}
		//			)
		//		}
	}
	
	func registerDevice(token: String, sender: AnyObject?) {
		let REGISTRATION_URL = "\(SITE)/api/devices/register.json"
		var deviceType = UIDevice.currentDevice().model
		manager.GET(
			REGISTRATION_URL,
			parameters: ["token":token, "device_type":deviceType],
			success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in

			},
			failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in

			}
		)
	}
	
	func registerUser(name: String, email: String, password: String, sender: NewUserViewController) {
		let REGISTRATION_URL = "\(SITE)/api/users/create.json"
		manager.POST(
			REGISTRATION_URL,
			parameters: ["name":name, "email":email, "password":password],
			success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
				var success = responseObject.valueForKey("success") as! Bool
				if (success) {
					sender.didRegisterUser()
				}
				else {
					var errorMessage = responseObject.valueForKey("error") as! String
					sender.userRegistrationDidFail(errorMessage)
				}
			},
			failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
				sender.userRegistrationDidFail("Could not communicate with server. Please try again.")
			}
		)
	}
	
	func getLocations(var params: Dictionary<String,String>, sender: LocationsTableViewController!) {
		
        params.updateValue(getCurrentLanguage(), forKey:"language")
    
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
			self.manager.GET(
				self.LOCATIONS_URL as String,
				parameters: params,
				success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
					var locations = [] as [Location]
					for dict in (responseObject as! Array<Dictionary<String,AnyObject>>) {
						var location = Location()
						var aDict = dict as Dictionary
						location.setValuesForKeysWithDictionary(aDict)
						locations += [location]
					}
					if sender != nil {
						sender.setAllLocations(locations)
					}
					//self.delegate!.locations = locations
				},
				failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
					if (error.code == NSURLErrorCancelled) {
						// We cancel requests all the time, so ignore this one
					}
					else {
						sender.setCMSError(error)
					}
				}
			)
			true
		}
	}
	
	func getMoreLocations(params: Dictionary<String,String>, sender: LocationsTableViewController!) {
		manager.GET(
			LOCATIONS_URL as String,
			parameters: params,
			success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
				var locations = [] as [Location]
				for dict in (responseObject as! Array<Dictionary<String,AnyObject>>) {
					var location = Location()
					var aDict = dict as Dictionary
					location.setValuesForKeysWithDictionary(aDict)
					locations += [location]
				}
				if sender != nil {
					sender.addLocations(locations)
				}
				//self.delegate!.locations = locations
			},
			failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
				sender.setCMSError(error)
			}
		)
	}
	
	func getNearbyLocationsToMonitor(params: Dictionary<String,String>) {
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
			self.manager.GET(
				self.LOCATIONS_URL as String,
				parameters: params,
				success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
					var locations = [] as [Location]
					for dict in (responseObject as! Array<Dictionary<String,AnyObject>>) {
						var location = Location()
						var aDict = dict as Dictionary
						location.setValuesForKeysWithDictionary(aDict)
						locations += [location]
					}
					self.delegate!.locations = locations
				},
				failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
					//sender.setCMSError(error)
				}
			)
			true
		}
	}
	
	func getCategories(region: String, sender: CategoriesViewController!) {
		var CATEGORIES_URL = "\(SITE)/api/locations/categories.json"
        let language = getCurrentLanguage()
        
		manager.GET(
			CATEGORIES_URL,
			parameters: ["region":region, "language": language],
			success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
				var categories = [] as [EstablishmentCategory]
				for dict in (responseObject as! Array<Dictionary<String,AnyObject>>) {
					var category = EstablishmentCategory()
					var aDict = dict as Dictionary
					category.setValuesForKeysWithDictionary(aDict)
					categories += [category]
				}
				if sender != nil {
					sender.setAllCategories(categories)
				}
			},
			failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
				self.delegate!.setCMSError(error)
			}
		)
	}
	
	func getFavorites(sender: FavoritesViewController!) {
		var FAVORITES_URL = "\(SITE)/api/locations/favorites.json"
            manager.GET(
                FAVORITES_URL,
                parameters: ["language" : getCurrentLanguage()],
                success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
                    if responseObject != nil{
                        var locations = [] as [Location]
                        for dict in (responseObject as! Array<Dictionary<String,AnyObject>>) {
                            var location = Location()
                            var aDict = dict as Dictionary
                            location.setValuesForKeysWithDictionary(aDict)
                            locations += [location]
                        }
                        if sender != nil {
                            sender.setAllLocations(locations)
                        }
                    }
                    else
                    {
                        //nri error
                        //sender.setCMSError(NSError())
                        sender.setCMSError(NSError(domain: NSURLErrorDomain, code: NSURLErrorCannotOpenFile, userInfo: nil))
                    }
                },
                failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
                    sender.setCMSError(error)
                }
            )
	}
	
	func setFavorite(_location: Location, sender: CMSFavoritesDelegate) {
		var FAVORITE_URL = "\(SITE)/api/locations/\(_location.id)/favorite.json"
		
		manager.POST(
			FAVORITE_URL,
			parameters: [],
			success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
				sender.didSetFavorite(responseObject.valueForKey("success") as! Bool)
			},
			failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
				sender.setCMSError(error)
				sender.didSetFavorite(false)
			}
		)
	}
	
	func deleteFavorite(_location: Location, sender: CMSFavoritesDelegate) {
		var FAVORITE_URL = "\(SITE)/api/locations/\(_location.id)/favorite.json"
		
		manager.DELETE(
			FAVORITE_URL,
			parameters: [],
			success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
				sender.didDeleteFavorite(responseObject.valueForKey("success") as! Bool)
			},
			failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
				sender.setCMSError(error)
				sender.didDeleteFavorite(false)
			}
		)
	}
	
	func checkin(_location: Location, type: String, sender: CheckInViewController) {
		var CHECKIN_URL = "\(SITE)/api/locations/\(_location.id)/check_in.json"
		
		manager.POST(
			CHECKIN_URL,
			parameters: ["type" : type],
			success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
				var responseDict = responseObject as! NSDictionary
				var success = responseObject.valueForKey("success") as! Bool
				if (success) {
					var totalPoints = responseObject.valueForKey("total_points") as? Int
					sender.didCheckin(true, message: nil, totalPoints: totalPoints!)
				}
				else {
					sender.didCheckin(false, message: responseObject.valueForKey("error") as! String?, totalPoints: 0)
				}
				
			},
			failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
				//sender.setCMSError(error)
				sender.didCheckin(false, message: error.localizedDescription, totalPoints: 0)
			}
		)
	}
	
	func getRewards(sender: RewardsViewController!) {
		var REWARDS_URL = "\(SITE)/api/rewards.json"
		manager.GET(
			REWARDS_URL,
			parameters: [],
			success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
				var responseDict = responseObject as! NSDictionary
				var availablePoints = responseDict["available_points"] as! Int
				var leaders = responseDict["leaders"] as! Array<Dictionary<String,AnyObject>>
				var rewardDicts = responseDict["rewards"] as! Array<Dictionary<String,AnyObject>>
				var rewards = [] as Array<Reward>
				for dict in (rewardDicts as Array<Dictionary<String,AnyObject>>) {
					var reward = Reward()
					reward.setValuesForKeysWithDictionary(dict)
					rewards += [reward]
				}
				sender.setAllAvailablePoints(availablePoints)
				sender.setAllLeaders(leaders)
				sender.setAllRewards(rewards)
			},
			failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
				sender.setCMSError(error)
			}
		)
	}
	
	func redeem(reward: Reward, params: [String:String], sender: RedeemViewController) {
		var REDEEM_URL = "\(SITE)/api/rewards/\(reward.id)/redeem.json"
		manager.POST(
			REDEEM_URL,
			parameters: params,
			success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
				var success = responseObject.valueForKey("success") as! Bool
				var message = responseObject.valueForKey("error") as? String
				if (message == nil) {
					message = ""
				}
				sender.didRedeem(success, message: message!)
			},
			failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
				sender.didRedeem(false, message: "We're sorry, an unexpected error occurred.  Please try again.")
			}
		)
	}
	
	func getAds(sender: AppDelegate?) {
		var ADS_URL = "\(SITE)/api/ads.json?link=1"
		manager.GET(
			ADS_URL,
			parameters: [],
			success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
				var ads = [] as [Ad]
				for dict in (responseObject as! Array<Dictionary<String,AnyObject>>) {
					var aDict = dict as Dictionary
					var ad = Ad()
					ad.setValuesForKeysWithDictionary(aDict)
					ads += [ad]
				}
				if (sender != nil) {
					sender!.setAllAds(ads)
				}
			},
			failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
				//sender.setCMSError(error)
			}
		)
	}
    func getAlerts(completion:(responseData:AnyObject?, error:NSError?)->()) {
        var ALERTS_URL = "\(SITE)/api/alerts.json?"
        let lastAlertID = NSUserDefaults.standardUserDefaults().objectForKey("LAST_ALERT_ID") as? Int
        var parameters = [String:AnyObject?]()
        if lastAlertID != nil{
            parameters["last_id"] = lastAlertID!
        }else{
            parameters["since"] = String(format: "%d", NSDate.timeIntervalSinceReferenceDate())
        }
        manager.GET(
            ALERTS_URL,
            parameters: [],
            success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
                
                print("Alert response: \(responseObject)")
                var alerts = [] as [Alert]
                for dict in (responseObject as! Array<Dictionary<String,AnyObject>>) {
                    var aDict = dict as Dictionary
                    var alert = Alert()
                    alert.setValuesForKeysWithDictionary(aDict)
                    alerts += [alert]
                }
                let lastAlert = alerts.last
                NSUserDefaults.standardUserDefaults().setObject(lastAlert?.id, forKey: "LAST_ALERT_ID")
                completion(responseData: alerts,error: nil)
            },
            failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
                //sender.setCMSError(error)
                completion(responseData: nil,error: error)

            }
        )
    }
    func deleteAlert(_location: Location, sender: CMSFavoritesDelegate) {
        var FAVORITE_URL = "\(SITE)/api/locations/\(_location.id)/favorite.json"
        
        manager.DELETE(
            FAVORITE_URL,
            parameters: [],
            success: { (operation: AFHTTPRequestOperation!,responseObject: AnyObject!) in
                sender.didDeleteFavorite(responseObject.valueForKey("success") as! Bool)
            },
            failure: { (operation: AFHTTPRequestOperation!,error: NSError!) in
                sender.setCMSError(error)
                sender.didDeleteFavorite(false)
            }
        )
    }
	
	func cancelRequests() {
		manager.operationQueue.cancelAllOperations()
	}
	
	func forgotPassword() {
		var PASSWORD_URL = "\(SITE)/users/password/new"
		UIApplication.sharedApplication().openURL(NSURL(string: PASSWORD_URL)!)
	}
    
    func getCurrentLanguage()->String{
        let lan = NSLocale.preferredLanguages()[0] as! String
        let language = (lan  == "ja") ? "Japanese" : "" //hardcosing now, since only supports Japanese
        return language
    }
}

protocol CMSConnectionDelegate {
	
	var locations: [Location] { get set }
	
	func setCMSError(error: NSError)
	// func setLocations(_locations: Array<Location>)
	// func didSetFavorite(_favorite: Bool)
}

protocol CMSLoginDelegate {
	func loginSucceeded()
	func loginFailed()
}

protocol CMSFavoritesDelegate {
	func setCMSError(error: NSError)
	func didSetFavorite(_status: Bool)
	func didDeleteFavorite(_status: Bool)
}
extension Dictionary {
    mutating func merge<K, V>(dict: [K: V]){
        for (k, v) in dict {
            self.updateValue(v as! Value, forKey: k as! Key)
        }
    }
}