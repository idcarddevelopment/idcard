//
//  LocationDetailViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 9/25/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import AddressBook

class LocationDetailViewController: UIViewController, CMSFavoritesDelegate {
    
    var location: Location = Location()
    var appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    var lastCheckinType: String = ""
    var lastCheckinPoints = 0
    var lastTotalPoints = 0
    
    @IBOutlet weak var bannerView: UIImageView!
    @IBOutlet weak var bannerIndicator: UIActivityIndicatorView!
    @IBOutlet weak var descriptionView: UITextView!
    @IBOutlet weak var disclaimerLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var categoriesLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    
    @IBOutlet weak var favoriteButton: IDCButton!
    @IBOutlet weak var websiteButton: IDCButton!
    @IBOutlet weak var checkinButton: IDCButton!
    @IBOutlet weak var mapButton: IDCButton!
    @IBOutlet weak var callButton: IDCButton!
    
    @IBOutlet weak var bannerHeight: NSLayoutConstraint!
    @IBOutlet weak var descriptionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var contentWidth: NSLayoutConstraint!
    @IBOutlet weak var addressHeight: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        applyStyles(true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        var loc = self.location
        //bannerView.image = location.logo
        //nameLabel.text = location.name
        descriptionView.text = location.discount
        
        
        var attributedDiscount = NSMutableAttributedString(string: "\(location.name)\n", attributes: [NSFontAttributeName: UIFont.boldSystemFontOfSize(17.0), NSForegroundColorAttributeName: UIColor.whiteColor()])
        attributedDiscount.appendAttributedString(NSMutableAttributedString(string: "\n", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(6.0), NSForegroundColorAttributeName: UIColor.whiteColor()]))
        attributedDiscount.appendAttributedString(NSMutableAttributedString(string: location.discount ?? "", attributes: [NSFontAttributeName: UIFont.systemFontOfSize(14.0), NSForegroundColorAttributeName: UIColor.whiteColor()]))
        descriptionView.attributedText = attributedDiscount
        descriptionView.textAlignment = NSTextAlignment.Center
        
        descriptionView.selectable = false //setting to false in Interface Builder causes all styling to disappear
        disclaimerLabel.text = location.disclaimer
        if (appDelegate.currentLocation != nil) {
            distanceLabel.text = location.formattedDistance()
        }
        else {
            distanceLabel.text = ""
        }
        categoriesLabel.text = location.categories.combine(", ")
        if (location.secondary_address == " ") {
            addressLabel.text = location.street_address
        }
        else {
            addressLabel.text = "\(location.street_address)\n\(location.secondary_address)"
        }
        cityLabel.text = "\(location.city), \(location.state) \(location.postal_code)"
        
        //self.checkinView = NSBundle.mainBundle().loadNibNamed("CheckInView", owner: self, options: nil)[0] as UIView
        
        
        if location.phoneURL() == nil || !UIApplication.sharedApplication().canOpenURL(location.phoneURL()!) {
            callButton.enabled = false
        }
        
        if location.websiteURL() == nil || !UIApplication.sharedApplication().canOpenURL(location.websiteURL()!) {
            websiteButton.enabled = false
        }
        
        if location.favorite {
            favoriteButton.selected = true
        }
        
        if (currentDistance() > 200.0 || !loc.can_check_in) {
            //FIXME: remove commenting
            //checkinButton.enabled = false
        }
        
        favoriteButton.enabled = true
        checkinButton.enabled = true
        enableCheckinButton()
        
        
        if (loc.banner_url != "") {
            if (loc.banner == nil) {
                bannerIndicator.startAnimating()
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                    var newBanner = UIImage(data: NSData(contentsOfURL: NSURL(string: loc.banner_url)!)!)
                    dispatch_async(dispatch_get_main_queue()) {
                        loc.banner = newBanner
                        self.bannerIndicator.stopAnimating()
                        self.bannerView.image = loc.banner
                    }
                }
                
            }
            else {
                self.bannerView.image = loc.banner
            }
        }
        
        //self.title = location.name
    }
    
    func enableCheckinButton(){
        
        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        if (appDelegate.cmsConnection!.loggedIn) {
            let user = delegate.cmsConnection?.username!
            
            var todayCheckins = NSUserDefaults.standardUserDefaults().integerForKey("TodayCheckins\(user)") ?? 0
            let lastCheckinTime = NSUserDefaults.standardUserDefaults().objectForKey("LastCheckinTime\(user)" ) as? NSDate
            
            var interval = 3600.0 //default interval 1hr
            if let lastDate = lastCheckinTime{
                interval = NSDate().timeIntervalSinceDate(lastDate)
                let calendar = NSCalendar.currentCalendar()
                //nri error
                //let comp = calendar.components(NSCalendarUnit.DayCalendarUnit, fromDate: lastCheckinTime!, toDate: NSDate(), options: NSCalendarOptions())
                
                let comp = calendar.components(NSCalendarUnit.Day, fromDate: lastCheckinTime!, toDate: NSDate(), options: NSCalendarOptions())
                if comp.day >= 1 {
                    todayCheckins = 0
                    NSUserDefaults.standardUserDefaults().setInteger(todayCheckins, forKey: "TodayCheckins\(user)")
                }
                
            }
            
            if todayCheckins < 3 && interval >= 3600{
                self.checkinButton.enabled = true
            }else{
                self.checkinButton.enabled = false
                
            }
        }
        
        //        if todayCheckins < 3
    }
    override func viewWillLayoutSubviews() {
        var height = self.view.frame.size.height
        //		if (height < 568) {
        //			var ratio = NSLayoutConstraint(item: bannerView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: bannerView, attribute: NSLayoutAttribute.Height, multiplier: 3.0, constant: 0.0)
        //			bannerView.addConstraints([ratio])
        //		}
        //		else {
        //			var ratio = NSLayoutConstraint(item: bannerView, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: bannerView, attribute: NSLayoutAttribute.Height, multiplier: 3.0, constant: 0.0)
        //			bannerView.addConstraints([ratio])
        //		}
        
        descriptionHeight.constant = descriptionView.sizeThatFits(CGSizeMake(descriptionView.frame.size.width, CGFloat(MAXFLOAT))).height
        
        //		if (bannerView.image != nil) {
        //			bannerHeight.constant = min(bannerView.image!.size.height/2.0, bannerView.image!.size.height * bannerView.frame.size.width / bannerView.image!.size.width)
        //		}
        
        contentWidth.constant = self.view.frame.size.width
        
        //		addressHeight.constant = addressLabel.sizeThatFits(CGSizeMake(addressLabel.frame.size.width, CGFloat(MAXFLOAT))).height
        
    }
    
    // MARK: - IBActions
    
    @IBAction func phone(sender: AnyObject) {
        UIApplication.sharedApplication().openURL(location.phoneURL()!)
    }
    
    @IBAction func showWebsite(sender: AnyObject) {
        UIApplication.sharedApplication().openURL(location.websiteURL()!)
    }
    
    @IBAction func showMap(sender: AnyObject) {
        
        
        var coordinate = CLLocationCoordinate2DMake(location.latitude, location.longitude)
        //nri error
        /*var placemark = MKPlacemark(coordinate: coordinate,
        addressDictionary: [kABPersonAddressStreetKey : location.street_address + " " + location.secondary_address, kABPersonAddressCityKey : location.city, kABPersonAddressStateKey : location.state, kABPersonAddressZIPKey : location.postal_code, kABPersonAddressCountryCodeKey : location.country])
        */
        
        //ajith error
        let addressDictionary = [String(kABPersonAddressStreetKey): location.street_address + " " + location.secondary_address,
            String(kABPersonAddressCityKey): location.city,
            String(kABPersonAddressStateKey): location.state,
            String(kABPersonAddressZIPKey): location.postal_code,
            String(kABPersonAddressCountryCodeKey): location.country,
        ]
        var placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addressDictionary)
        
        

        

        
        var mapItem = MKMapItem(placemark: placemark)
        mapItem.name = location.name
        mapItem.openInMapsWithLaunchOptions(nil)
    }
    
    @IBAction func favorite(sender: AnyObject) {
        
        if (appDelegate.cmsConnection!.loggedIn) {
            favoriteButton.enabled = false
            if location.favorite {
                appDelegate.cmsConnection!.deleteFavorite(location, sender: self)
            }
            else {
                appDelegate.cmsConnection!.setFavorite(location, sender: self)
            }
        }else{
            let alert = UIAlertView(title: "It is required that you sign in to use this feature.".localized, message: nil, delegate: nil, cancelButtonTitle: "OK")
            alert.show()
        }
    }
    
    @IBAction func checkin(sender: AnyObject) {
        //		checkinButton.enabled = false
        //		appDelegate.cmsConnection!.checkin(location, sender: self)
        
        //		var alertViewController = CustomAlertView()
        //		alertViewController.alertView = NSBundle.mainBundle().loadNibNamed("CheckInView", owner: self, options: nil)[0] as UIView
        //
        //		alertViewController.delegate = self
        //		alertViewController.show()
        
        //		showCheckInView()
    }
    
    // MARK: - CMSConnectionDelegate
    
    func setCMSError(error: NSError) {
        UIAlertView(title: "Error".localized, message: error.localizedDescription, delegate: self, cancelButtonTitle: "OK").show()
    }
    
    func didSetFavorite(_status: Bool) {
        appDelegate.favoritesHaveChanged = true
        favoriteButton.enabled = true
        if _status {
            location.favorite = true
            favoriteButton.selected = true
        }
    }
    
    func didDeleteFavorite(_status: Bool) {
        appDelegate.favoritesHaveChanged = true
        favoriteButton.enabled = true
        if _status {
            location.favorite = false
            favoriteButton.selected = false
        }
    }
    
    func didCheckin(_status: Bool, message: String?) {
        checkinButton.enabled = true
        if _status {
            checkinButton.selected = true
        }
        else {
            checkinButton.selected = false
            UIAlertView(title: "", message: message, delegate: self, cancelButtonTitle: "OK").show()
        }
    }
    
    // MARK: - Navigation
    
    override func shouldPerformSegueWithIdentifier(identifier: String?, sender: AnyObject?) -> Bool {
        if (identifier == "checkin") &&  !appDelegate.cmsConnection!.loggedIn{
            
            let alert = UIAlertView(title: "It is required that you sign in to use this feature.".localized, message: nil, delegate: nil, cancelButtonTitle: "OK")
            alert.show()
            
            return false
        }
        return true
    }
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if (segue.identifier == "checkin") {
            
            if appDelegate.cmsConnection!.loggedIn{
                var viewController = segue.destinationViewController as! CheckInViewController
                viewController.location = self.location
                viewController.locationViewController = self
            }else{
                let alert = UIAlertView(title: "It is required that you sign in to use this feature.".localized, message: nil, delegate: nil, cancelButtonTitle: "OK")
                alert.show()
            }
        }
        else if (segue.identifier == "checkin_success") {
            var viewController = segue.destinationViewController as! CheckinSuccessViewController
            viewController.checkinType = lastCheckinType
            viewController.checkinPoints = lastCheckinPoints
            viewController.totalPoints = lastTotalPoints
        }
    }
    
    func showCheckInView() {
        var checkinViewController = CheckInViewController(nibName: "CheckInViewController", bundle: NSBundle.mainBundle())
        //nri error
        //checkinViewController.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen;
        if #available(iOS 8.0, *) {
            checkinViewController.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        } else {
            // Fallback on earlier versions
        };
        
        checkinViewController.modalTransitionStyle = UIModalTransitionStyle.CoverVertical;
        
        self.presentViewController(checkinViewController, animated: true, completion: nil)
    }
    
    func currentDistance() -> CLLocationDistance { // in meters
        if (appDelegate.currentLocation != nil) {
            return appDelegate.currentLocation!.distanceFromLocation(CLLocation(latitude: location.latitude, longitude: location.longitude))
        }
        else {
            return Double.infinity
        }
    }
    
    func showSuccessPage() {
        
        
        self.performSegueWithIdentifier("checkin_success", sender: nil)
    }
    
    
}
