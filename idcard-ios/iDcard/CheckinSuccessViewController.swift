//
//  CheckinSuccessViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 11/24/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class CheckinSuccessViewController: UIViewController {
	
	var checkinType: String = ""
	var checkinPoints: Int = 0
	var totalPoints = 0
	
	@IBOutlet weak var checkinTypeLabel: UILabel!
	@IBOutlet weak var checkinPointsLabel: UILabel!
	@IBOutlet weak var totalPointsView: UIView!
	@IBOutlet weak var totalPointsLabel: UILabel!

	override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
	
	override func viewWillAppear(animated: Bool) {
		checkinTypeLabel.text = checkinType.localized
		checkinPointsLabel.text = "\(checkinPoints)"
        let totalText = "Total Points".localized
        totalPointsLabel.text = " \(totalText): \(totalPoints)"
		
		totalPointsView.layer.cornerRadius = 12.0
		totalPointsView.layer.borderWidth = 1.0;
		totalPointsView.layer.borderColor = UIColor.highlightColor().CGColor
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
	@IBAction func dismiss(sender: AnyObject) {
		self.dismissViewControllerAnimated(true, completion: nil)
	}
	
	/*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
