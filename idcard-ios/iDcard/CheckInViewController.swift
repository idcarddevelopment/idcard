//
//  CheckInViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/1/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class CheckInViewController: UIViewController, UIAlertViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIActionSheetDelegate, FacebookCheckInController, UITextViewDelegate {
	
	var locationViewController: LocationDetailViewController?
	var location: Location?
	var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    var cameraImagePicker: UIImagePickerController?
    var libraryImagePicker: UIImagePickerController?
    var imagePickerSheet: UIActionSheet?
	var selectedPhoto: UIImage?
	
	var shouldShowSuccess = false
	var checkinType = ""
	var checkinPoints = 0
	var totalPoints = 0
	
	@IBOutlet weak var regularCheckInButton: UIView!
    @IBOutlet weak var facebookCheckInButton: UIView!
    @IBOutlet weak var facebookPhotoCheckInButton: UIView!
    @IBOutlet weak var facebookCheckInButtonButton: UIButton!
    @IBOutlet weak var facebookPhotoCheckInButtonButton: UIButton!
	@IBOutlet weak var waitingView: UIView!
	@IBOutlet weak var checkmarkView: UIImageView!
	@IBOutlet weak var activityView: UIActivityIndicatorView!
    @IBOutlet weak var facebookLabel: UILabel!
    @IBOutlet weak var facebookLabelHeight: NSLayoutConstraint!

	@IBOutlet weak var facebookMessageView: UIView!
	@IBOutlet weak var facebookMessageTextView: UITextView!
	@IBOutlet weak var facebookMessageLabel: UILabel!
	
	@IBOutlet weak var facebookPhotoMessageView: UIView!
	@IBOutlet weak var facebookPhotoMessageTextView: UITextView!
	@IBOutlet weak var facebookPhotoMessageLabel: UILabel!
	
    @IBOutlet weak var imageView: UIImageView!
	override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		
		regularCheckInButton.layer.cornerRadius = 12.0
		regularCheckInButton.layer.borderWidth = 1.0;
		regularCheckInButton.layer.borderColor = UIColor.highlightColor().CGColor

		facebookCheckInButton.layer.cornerRadius = 12.0
		facebookCheckInButton.layer.borderWidth = 1.0;
		facebookCheckInButton.layer.borderColor = UIColor.highlightColor().CGColor

		facebookPhotoCheckInButton.layer.cornerRadius = 12.0
		facebookPhotoCheckInButton.layer.borderWidth = 1.0;
		facebookPhotoCheckInButton.layer.borderColor = UIColor.highlightColor().CGColor
		
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
            cameraImagePicker = UIImagePickerController()
            cameraImagePicker!.delegate = self
            cameraImagePicker!.allowsEditing = false
            cameraImagePicker!.sourceType = UIImagePickerControllerSourceType.Camera
            //nri error
            //cameraImagePicker!.mediaTypes = [kUTTypeImage]
            cameraImagePicker!.mediaTypes = [kUTTypeImage as String]
        }
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.PhotoLibrary)) {
            libraryImagePicker = UIImagePickerController()
            libraryImagePicker!.delegate = self
            libraryImagePicker!.allowsEditing = false
            libraryImagePicker!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
            //nri error
            //libraryImagePicker!.mediaTypes = [kUTTypeImage]
            libraryImagePicker!.mediaTypes = [kUTTypeImage as String]
        }

        imagePickerSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: "Cancel".localized, destructiveButtonTitle: nil, otherButtonTitles: "Camera".localized, "Gallery".localized)
		
}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		
		checkmarkView.hidden = true
		activityView.startAnimating()
        
        //if (FBSession.activeSession().isOpen) {
         //  facebookCheckInButton.alpha = 1.0
         //   facebookPhotoCheckInButton.alpha = 1.0
           // facebookCheckInButtonButton.enabled = true
            //facebookPhotoCheckInButtonButton.enabled = true
//          //  facebookLabelHeight.constant = 0.0
        //}
        //else {
          //  facebookCheckInButton.alpha = 0.5
          //  facebookPhotoCheckInButton.alpha = 0.5
          //  facebookCheckInButtonButton.enabled = false
          //  facebookPhotoCheckInButtonButton.enabled = false
//            facebookLabelHeight.constant = 41.0
       // }
		
		shouldShowSuccess = false
	}
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

	@IBAction func regularCheckIn(sender: AnyObject) {
		
		checkinType = "Regular Check-In"
		checkinPoints = 2
		
		waitingView.hidden = false
		appDelegate.cmsConnection!.checkin(location!, type: "regular", sender: self)
	}
	
	@IBAction func facebookCheckIn(sender: AnyObject) {
    
		waitingView.hidden = false
		facebookMessageTextView.resignFirstResponder()
		if (FBSession.activeSession().isOpen) {
			var x = FBSession.activeSession()
			if ((FBSession.activeSession().permissions as NSArray).indexOfObject("publish_actions") == NSNotFound) {
				FBSession.activeSession().requestNewPublishPermissions(["publish_actions"], defaultAudience: FBSessionDefaultAudience.Everyone,
					completionHandler: { (session: FBSession!, error: NSError!) in
						self.publishFacebookStatus()
					}
				)
			}
			else {
				self.publishFacebookStatus()
			}

		}
		else {
			var permissions = ["publish_actions"]
			FBSession.openActiveSessionWithPublishPermissions(permissions,
				defaultAudience: FBSessionDefaultAudience.Everyone,
				allowLoginUI: true,
				completionHandler: { (session: FBSession!, status: FBSessionState, error: NSError!) in
					if (error != nil) {
						
					}
					else if (status == FBSessionState.Open) {
						self.publishFacebookStatus()
					}
				}
			)
		}
	}

	@IBAction func facebookPhotoCheckIn(sender: AnyObject) {
		waitingView.hidden = false
        if (imagePickerSheet != nil) {
            imagePickerSheet!.showInView(self.view)
        }
		//self.presentViewController(cameraImagePicker, animated: true, completion: nil)
	}
	
	@IBAction func dismiss(sender: AnyObject) {
		self.dismiss()
	}
	
	func dismiss() {
		waitingView.hidden = true
		self.dismissViewControllerAnimated(true, completion: nil)
		if (shouldShowSuccess && locationViewController != nil) {
			locationViewController!.lastCheckinType = checkinType
			locationViewController!.lastCheckinPoints = checkinPoints
			locationViewController!.lastTotalPoints = totalPoints
			locationViewController!.showSuccessPage()
		}
	}
	
	func didCheckin(_status: Bool, message: String?, totalPoints: Int) {
		if _status {
			activityView.stopAnimating()
			checkmarkView.hidden = false
			shouldShowSuccess = true
			self.totalPoints = totalPoints
            
            let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let userName = delegate.cmsConnection?.username!
            
            var todayCheckins = NSUserDefaults.standardUserDefaults().integerForKey("TodayCheckins\(userName)") ?? 0
            todayCheckins = todayCheckins + 1
            let defaults = NSUserDefaults.standardUserDefaults()
            defaults.setInteger(todayCheckins, forKey: "TodayCheckins\(userName)")
            defaults.setObject(NSDate(), forKey: "LastCheckinTime\(userName)")
            defaults.synchronize()
			var timer = NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: "dismiss", userInfo: nil, repeats: false)
			//self.dismissViewControllerAnimated(true, completion: nil)
		}
		else {
			waitingView.hidden = true
			UIAlertView(title: "", message: message, delegate: self, cancelButtonTitle: "OK").show()
		}
	}
	
	@IBAction func showFacebookStatusFields(sender: AnyObject) {
        
        self.publishFacebookStatus()
//		facebookMessageView.alpha = 0.0
//		facebookMessageView.hidden = false
//		UIView.animateWithDuration(0.5,
//			animations: { () in
//				self.facebookMessageView.alpha = 1.0
//			},
//			completion: { (finished) in
//				self.facebookMessageView.alpha = 1.0
//			}
//		)

	}
	
	@IBAction func hideFacebookStatusFields(sender: AnyObject) {
		facebookMessageTextView.resignFirstResponder()
		UIView.animateWithDuration(0.5,
			animations: { () in
				self.facebookMessageView.alpha = 0.0
			},
			completion: { (finished) in
				self.facebookMessageView.hidden = true
			}
		)
		
	}
	
	@IBAction func showFacebookPhotoStatusFields(sender: AnyObject) {
		facebookPhotoMessageView.alpha = 0.0
		facebookPhotoMessageView.hidden = false
        self.imageView.image = self.selectedPhoto
		UIView.animateWithDuration(0.5,
			animations: { () in
				self.facebookPhotoMessageView.alpha = 1.0
			},
			completion: { (finished) in
				self.facebookPhotoMessageView.alpha = 1.0
			}
		)
	}
	
	@IBAction func hideFacebookPhotoStatusFields(sender: AnyObject) {
		facebookPhotoMessageTextView.resignFirstResponder()
		UIView.animateWithDuration(0.5,
			animations: { () in
				self.facebookPhotoMessageView.alpha = 0.0
			},
			completion: { (finished) in
				self.facebookPhotoMessageView.hidden = true
			}
		)
		
	}
	
	func publishFacebookStatus() {

		checkinType = "Facebook Check-In"
		checkinPoints = 7
		
//		FBRequestConnection.startForPostStatusUpdate(self.checkInMessage(),
//			completionHandler: { (connection, result, error) in
//				if (error == nil) {
//					self.waitingView.hidden = false
//					var cmsConnection = self.appDelegate.cmsConnection
//					cmsConnection!.checkin(self.location!, type: "facebook", sender: self)
//				}
//				else {
//					UIAlertView(title: "Facebook Error", message: "Sorry, we couldn't communicate with Facebook. Please try again.", delegate: self, cancelButtonTitle: "OK").show()
//                    self.waitingView.hidden = true
//				}
//			}
//		)
		var opengraphURL = location!.banner_url as NSString
		opengraphURL = opengraphURL.stringByReplacingOccurrencesOfString("/thumb/", withString: "/opengraph/")
		//FacebookAdditions.checkIn("islediscountcard:sponsor", title: location!.name, image: opengraphURL, url: "http://app.theidcard.com/api/locations/\(location!.id)/og", description: self.facebookMessageTextView.text, checkInViewController: self)
		FacebookAdditions.checkIn("islediscountcard:sponsor", title: location!.name, image: opengraphURL as String, url: "https://www.facebook.com/islediscountcard", description: self.checkInMessage(), checkInViewController: self)
		//var object: FBGraphObject = FBGraphObject.openGraphActionForPost()
		//var object = FBGraphObject.openGraphObjectForPostWithType("islediscountcard:sponsor", title: location!.name, image: location!.logo_url, url: "http://app.theidcard.com/api/locations/\(location!.id)/og", description: self.checkInMessage()) as FBOpenGraphObject
//		FBRequestConnection.startForPostOpenGraphObject(object, completionHandler: { (connection, result, error) in
//			if (error == nil) {
//				self.waitingView.hidden = false
//				var cmsConnection = self.appDelegate.cmsConnection
//				cmsConnection!.checkin(self.location!, type: "facebook", sender: self)
//			}
//			else {
//				UIAlertView(title: "Facebook Error", message: "Sorry, we couldn't communicate with Facebook. Please try again.", delegate: self, cancelButtonTitle: "OK").show()
//				self.waitingView.hidden = true
//			}
//		})
//		FBRequestConnection.startForPostWithGraphPath("me/islediscountcard:check_in_at", graphObject: object, completionHandler: { (connection, result, error) in
//			if (error == nil) {
//				var resultObj = result as FBGraphObject
//				self.waitingView.hidden = false
//				var cmsConnection = self.appDelegate.cmsConnection
//				cmsConnection!.checkin(self.location!, type: "facebook", sender: self)
//			}
//			else {
//				UIAlertView(title: "Facebook Error", message: "Sorry, we couldn't communicate with Facebook. Please try again.", delegate: self, cancelButtonTitle: "OK").show()
//				self.waitingView.hidden = true
//			}
//		})
		
		
//		NSMutableDictionary<FBGraphObject> *object =
//			[FBGraphObject openGraphObjectForPostWithType:@business.business
//		title:@Sample Business
//		image:@https://fbstatic-a.akamaihd.net/images/devsite/attachment_blank.png
//		url:@http://samples.ogp.me/616004095080596
//		description:@];;
//		
//		[FBRequestConnection startForPostWithGraphPath:@"me/objects/business.business"
//		graphObject:object
//		completionHandler:^(FBRequestConnection *connection,
//		id result,
//		NSError *error) {
//		// handle the result
//		}];

	}
    
    func didPublishFacebookStatus(success: Bool) {
        if (success) {
            self.waitingView.hidden = false
            var cmsConnection = self.appDelegate.cmsConnection
            
            if checkinType == "Facebook Photo Check-In"{
                cmsConnection!.checkin(self.location!, type: "photo", sender: self)
            }else{
                cmsConnection!.checkin(self.location!, type: "facebook", sender: self)
            }
        }
        else {
            UIAlertView(title: "Facebook Error", message: "Sorry, we couldn't communicate with Facebook. Please try again.", delegate: self, cancelButtonTitle: "OK").show()
            self.waitingView.hidden = true
        }
    }
    
    func publishFacebookPhoto(image: UIImage) {
        
        checkinType = "Facebook Photo Check-In"
		checkinPoints = 5
		
		var opengraphURL = location!.logo_url as NSString
		opengraphURL = opengraphURL.stringByReplacingOccurrencesOfString("/thumb/", withString: "/opengraph/")
		//FacebookAdditions.checkIn("islediscountcard:sponsor", title: location!.name, image: opengraphURL, url: "http://app.theidcard.com/api/locations/\(location!.id)/og", description: self.facebookMessageTextView.text, checkInViewController: self)
        FacebookAdditions.photoCheckIn("islediscountcard:sponsor", title: location!.name, image: opengraphURL as String, url: "https://www.facebook.com/islediscountcard", description: self.facebookPhotoMessageTextView.text, photo: image, checkInViewController: self)
        

//		var params = ["message": self.checkInMessage(), "picture" : UIImagePNGRepresentation(image)]
//		FBRequestConnection.startWithGraphPath("me/photos", parameters: params, HTTPMethod: "POST",
//			completionHandler: { (connection, result, error) in
//				if (error == nil) {
//					self.waitingView.hidden = false
//					var cmsConnection = self.appDelegate.cmsConnection
//					cmsConnection!.checkin(self.location!, type: "facebook-photo", sender: self)
//				}
//				else {
//					UIAlertView(title: "Facebook Error", message: "Sorry, we couldn't communicate with Facebook. Please try again.", delegate: self, cancelButtonTitle: "OK").show()
//				}
//			}
//		)
	}
	
	func currentPermissions() {
		FBRequestConnection.startWithGraphPath("/me/permissions", completionHandler: { (connection: FBRequestConnection!, result: AnyObject!, error: NSError!) in
			
		})
	}
	
	func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]!) {
		picker.dismissViewControllerAnimated(true, completion: nil)
		self.selectedPhoto = image
//		self.showFacebookPhotoStatusFields(self)
        self.publishFacebookPhoto(image)
//        facebookPhotoCheckinButtonTapped()
    }
	
    func imagePickerControllerDidCancel(picker: UIImagePickerController){
        self.waitingView.hidden = true
        picker.dismissViewControllerAnimated(true, completion: nil)

    }

	@IBAction func facebookPhotoCheckinButtonTapped() {
//		waitingView.hidden = false
//		facebookPhotoMessageTextView.resignFirstResponder()
//		if (FBSession.activeSession().isOpen) {
//			var x = FBSession.activeSession()
//			if ((FBSession.activeSession().permissions as NSArray).indexOfObject("publish_actions") == NSNotFound) {
//				FBSession.activeSession().requestNewPublishPermissions(["publish_actions"], defaultAudience: FBSessionDefaultAudience.Everyone,
//					completionHandler: { (session: FBSession!, error: NSError!) in
//						self.publishFacebookPhoto(self.selectedPhoto!)
//					}
//				)
//			}
//			else {
//				self.publishFacebookPhoto(self.selectedPhoto!)
//			}
//			
//		}
//		else {
//			var permissions = ["publish_actions"]
//			FBSession.openActiveSessionWithPublishPermissions(permissions,
//				defaultAudience: FBSessionDefaultAudience.Everyone,
//				allowLoginUI: true,
//				completionHandler: { (session: FBSession!, status: FBSessionState, error: NSError!) in
//					if (error != nil) {
//						
//					}
//					else if (status == FBSessionState.Open) {
//						self.publishFacebookPhoto(self.selectedPhoto!)
//					}
//				}
//			)
//		}
	}
	
	func checkInMessage() -> String {
		if (facebookMessageTextView.text.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()) == "") {
			return "Saving money with my iDcard.  Living the good life for less."
		}
		else {
			return facebookMessageTextView.text
		}
		//return "I'm using my iDcard at \(location!.name)"
		//
	}
	
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        if (buttonIndex == 1) { // Take Photo
            if (cameraImagePicker == nil) {
                UIAlertView(title: "Camera not supported on this device", message: nil, delegate: nil, cancelButtonTitle: "OK").show()
                waitingView.hidden = true

            }
            else {
                self.presentViewController(cameraImagePicker!, animated: true, completion: nil)
            }
        }
        else if (buttonIndex == 2) { // Choose Photo
            if (libraryImagePicker == nil) {
                UIAlertView(title: "Photo Library not supported on this device", message: nil, delegate: nil, cancelButtonTitle: "OK").show()
                waitingView.hidden = true
            }
            else {
                self.presentViewController(libraryImagePicker!, animated: true, completion: nil)
            }
        }else{
            waitingView.hidden = true

        }
        
        
    }
	
	func textViewDidBeginEditing(textView: UITextView) {
		if (textView == facebookMessageTextView) {
			facebookMessageLabel.hidden = true
		}
		else if (textView == facebookPhotoMessageTextView) {
			facebookPhotoMessageLabel.hidden = true
		}
	}
	
	func textViewDidEndEditing(textView: UITextView) {
		if (textView == facebookMessageTextView) {
			facebookMessageLabel.hidden = false
		}
		else if (textView == facebookPhotoMessageTextView) {
			facebookPhotoMessageLabel.hidden = false
		}
	}
	
    func sharer(sharer: FBSDKSharing!, didCompleteWithResults results: [NSObject : AnyObject]!) {
        waitingView.hidden = true
        self.didPublishFacebookStatus(true)

    }
    func sharer(sharer: FBSDKSharing!, didFailWithError error: NSError!) {
        waitingView.hidden = true
        self.didPublishFacebookStatus(false)
    }
    func sharerDidCancel(sharer: FBSDKSharing!) {
        waitingView.hidden = true
    }
}
