//
//  RegionPickerViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/24/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class RegionPickerViewController: UIViewController {

	var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
	var categoriesViewController: CategoriesViewController?
	var locationsTableViewController: LocationsTableViewController?
	
	@IBOutlet weak var oahuButton: IDCButton!
	@IBOutlet weak var mauiButton: IDCButton!
	@IBOutlet weak var kauaiButton: IDCButton!
	@IBOutlet weak var bigIslandButton: IDCButton!
	@IBOutlet weak var nevadaButton: IDCButton!
	@IBOutlet weak var newJerseyButton: IDCButton!

	override func viewDidLoad() {
        super.viewDidLoad()

		applyStyles(false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	override func viewWillAppear(animated: Bool) {
		var currentRegion = appDelegate.currentRegion
		if (currentRegion == "Oahu") {
			oahuButton.selected = true
		}
		else if (currentRegion == "Maui") {
			mauiButton.selected = true
		}
		else if (currentRegion == "Kauai") {
			kauaiButton.selected = true
		}
		else if (currentRegion == "Big Island") {
			bigIslandButton.selected = true
		}
		else if (currentRegion == "Nevada") {
			nevadaButton.selected = true
		}
		else if (currentRegion == "New Jersey") {
			newJerseyButton.selected = true
		}
	}
    

	@IBAction func updateRegion(sender: AnyObject) {
		if (sender as! NSObject == oahuButton) {
			appDelegate.currentRegion = "Oahu"
		}
		else if (sender as! NSObject == mauiButton) {
			appDelegate.currentRegion = "Maui"
		}
		else if (sender as! NSObject == kauaiButton) {
			appDelegate.currentRegion = "Kauai"
		}
		else if (sender as! NSObject == bigIslandButton) {
			appDelegate.currentRegion = "Big Island"
		}
		else if (sender as! NSObject == nevadaButton) {
			appDelegate.currentRegion = "Nevada"
		}
		else if (sender as! NSObject == newJerseyButton) {
			appDelegate.currentRegion = "New Jersey"
		}
		if (categoriesViewController != nil) {
			categoriesViewController!.viewWillAppear(false)
			categoriesViewController!.updateCategories()
		}
		else if (locationsTableViewController != nil) {
			locationsTableViewController!.viewWillAppear(false)
			locationsTableViewController!.updateLocations()
		}
		self.dismissViewControllerAnimated(true, completion: nil)
	}
	
	
	
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
