//
//  Ad.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/28/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class Ad: NSObject {
	
	var id = 0
	var name = ""
	var ad_type = "image"
	var updated_at = "0000-01-01T00:00:00.000Z"
	var duration = 10.0
	var url = ""
	var link: String? = nil
	var image: UIImage?
    var url_sd :String = ""
    var url_ld :String = ""
	
	let documentsPath = NSSearchPathForDirectoriesInDomains(.CachesDirectory, .UserDomainMask, true)[0] as! NSString
	
	override init() {}
	
	required init(coder aDecoder: NSCoder) {
		self.id  = aDecoder.decodeObjectForKey("id") as! Int
		self.name  = aDecoder.decodeObjectForKey("name") as! String
		self.ad_type  = aDecoder.decodeObjectForKey("ad_type") as! String
		self.updated_at  = aDecoder.decodeObjectForKey("updated_at") as! String
		self.duration  = aDecoder.decodeObjectForKey("duration") as! Double
		self.url  = aDecoder.decodeObjectForKey("url") as! String
		self.link  = aDecoder.decodeObjectForKey("link") as? String
		//self.image  = aDecoder.decodeObjectForKey("image") as? UIImage // saving images in plists is really inefficient.  Videos even more so.
	}

	func encodeWithCoder(aCoder: NSCoder!) {
		aCoder.encodeObject(id, forKey:"id")
		aCoder.encodeObject(name, forKey:"name")
		aCoder.encodeObject(ad_type, forKey:"ad_type")
		aCoder.encodeObject(updated_at, forKey:"updated_at")
		aCoder.encodeObject(duration, forKey:"duration")
		aCoder.encodeObject(url, forKey:"url")
		aCoder.encodeObject(link, forKey:"link")
		//aCoder.encodeObject(image, forKey:"image")
	}
	
	func updateImage() {
		if (url != "" && ad_type == "image") {
			var imageData = NSData(contentsOfURL: NSURL(string: self.url)!)
			if (imageData != nil) {
				self.image = UIImage(data: imageData!)
				//imageData!.writeToFile(self.filePath(), atomically: true)
			}
		}
	}
	
	func filePath() -> String {
		if (self.ad_type == "image") {
			return documentsPath.stringByAppendingPathComponent("ads/\(self.id).jpg")
		}
		else {
			return documentsPath.stringByAppendingPathComponent("ads/\(self.id).m4v")
		}
	}
}
