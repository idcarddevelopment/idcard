//
//  AlertViewController.swift
//  iDcard
//
//  Copyright (c) 2015 DMG❖Bluegill. All rights reserved.
//

import UIKit

class AlertViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.rightBarButtonItem = self.editButtonItem()
        self.view.backgroundColor = UIColor.blackColor()
        self.tableView.backgroundColor = UIColor.clearColor()
        self.tableView.tableFooterView = UIView()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "reloadData", name: "AlertUpdateNotification", object: nil)
        applyStyles(true)

        
    }

    override func viewWillDisappear(animated: Bool) {
        Alert.saveAlertsToDefaults(appDelegate.alerts)
    }
    func reloadData(){
        self.tableView.reloadData()
        setBadge()
    }

    override func setEditing(editing: Bool, animated: Bool){
        
        super.setEditing(editing, animated: animated)
        self.tableView.setEditing(editing, animated: true)
    
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return appDelegate.alerts.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        //nri error
        //let cell = tableView.dequeueReusableCellWithIdentifier("AlertCell") as! UITableViewCell
        let cell = tableView.dequeueReusableCellWithIdentifier("AlertCell") as UITableViewCell!
        
        let dateLabel = cell.viewWithTag(100) as! UILabel
        let messageLabel = cell.viewWithTag(101) as! UILabel
        let formatter = NSDateFormatter()
        let alert = appDelegate.alerts[indexPath.row]

        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        let date = formatter.dateFromString(alert.updated_at)
        formatter.dateFormat = "MM-dd-yyyy"
        
        if alert.isRead == false{
            dateLabel.text = formatter.stringFromDate(date!).uppercaseString
            dateLabel.textColor = self.view.tintColor
            messageLabel.text = alert.title.uppercaseString
            messageLabel.textColor = self.view.tintColor
        }else{
            dateLabel.text = formatter.stringFromDate(date!)
            messageLabel.text = alert.title
            dateLabel.textColor = UIColor.whiteColor()
            messageLabel.textColor = UIColor.whiteColor()

        }
        return cell
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath){
        cell.contentView.backgroundColor = UIColor.clearColor()
        cell.backgroundColor = UIColor.clearColor()
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath){
        let alert = appDelegate.alerts[indexPath.row]
        alert.isRead = true
        let alertView = UIAlertView(title: "Message", message: alert.message, delegate: nil, cancelButtonTitle:"Close".localized)
        tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
        alertView.show()
        setBadge()
    }
    
    func setBadge(){
        let count:String?
        
        if appDelegate.getUnreadAlertCount() != 0{
            count = "\(appDelegate.getUnreadAlertCount())"
        }else{
            count = nil
        }
        self.navigationController?.tabBarItem.badgeValue = count
        appDelegate.setAppBadge()
        NSNotificationCenter.defaultCenter().postNotificationName("AlertReadNotification", object: nil)
    }
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {
            
            let alert = appDelegate.alerts[indexPath.row]
            appDelegate.alerts.removeAtIndex(indexPath.row)
            Alert.saveAlertsToDefaults(appDelegate.alerts)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
            setBadge()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
