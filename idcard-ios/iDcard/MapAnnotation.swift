//
//  MapAnnotation.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/16/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//
import UIKit
import MapKit

class MapAnnotation: NSObject, MKAnnotation {
	
	var coordinate: CLLocationCoordinate2D {
		get {
			return CLLocationCoordinate2DMake(location.latitude, location.longitude)
		}
	}
	//nri error
    //var title: String
	var title: String? {
		get {
			return location.name
		}
	}
    //nri error
    //var subtitle: String
	var subtitle: String? {
		get {
			return location.street_address
		}
	}
	
	var location = Location()
	
	
}
