//
//  AdvertisingViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/20/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class AdvertisingViewController: UIViewController {
	
	var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
	
	@IBOutlet weak var titleLabel: UILabel!
	
    override func viewDidLoad() {
        super.viewDidLoad()

		applyStyles(true)
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
	
	
	@IBAction func phoneCall(sender: AnyObject) {
		UIApplication.sharedApplication().openURL(NSURL(string: "tel:\(appDelegate.contactPhone)")!)
	}
	
	@IBAction func sendEmail(sender: AnyObject) {
		if (titleLabel.text == "Free Advertising") {
			UIApplication.sharedApplication().openURL(NSURL(string: "mailto:\(appDelegate.contactEmail)?subject=Free%20Advertising")!)
		}
		else {
			UIApplication.sharedApplication().openURL(NSURL(string: "mailto:\(appDelegate.contactEmail)?subject=Fundraiser%20Info")!)
		}
	}


}
