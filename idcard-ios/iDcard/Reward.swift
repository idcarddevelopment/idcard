//
//  Reward.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/7/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class Reward: NSObject {
	
	var name: String = ""
	var id: Int = 0
	var detail = ""
	var points: Int = 0
	var photo_url: String = "" {
		didSet {
			setPhotoFromURL()
		}
	}
	var photo: UIImage?
	var banner_url: String = ""
	var banner: UIImage?
    var inventory: Int = 0
	
	func setPhotoFromURL() {
		if (photo_url != "") {
			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
				var newPhoto = UIImage(data: NSData(contentsOfURL: NSURL(string: self.photo_url)!)!)
				
				dispatch_async(dispatch_get_main_queue()) {
					self.photo = newPhoto
				}
			}
		}
	}
	
}
