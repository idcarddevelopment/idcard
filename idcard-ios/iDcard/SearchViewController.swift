//
//  SearchViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/14/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class SearchViewController: LocationsTableViewController, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
	var submittedSearchText = ""
	var isSearching = false
//	var foundNoResults = false //Don't keep searching if the string already has no matches
//	var foundNoResultsString = ""
	
	// MARK: - Navigation
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
		self.tableView.tableHeaderView = self.searchBar;
		applyStyles(true)
	}
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		
		self.navigationItem.rightBarButtonItem = nil
		if (self.locations.count == 0) {
			self.tableView.scrollRectToVisible(self.tableView.tableHeaderView!.frame, animated: false)
		}
		self.title = nil
	}
	
//	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
//		var detailViewController = segue.destinationViewController as LocationDetailViewController
//		var selectedPath = self.tableView.indexPathForSelectedRow()!
//		detailViewController.location = self.locations[selectedPath.row]
//	}
	
	
	override func numberOfSectionsInTableView(tableView: UITableView?) -> Int {
		if (self.locations.count > 0) {
			self.tableView.backgroundView = nil;
			return 1;
		}
		else {
			var imageView = UIImageView(image: UIImage(named: "search-placeholder"))
			imageView.frame = CGRectMake(0.0, 0.0, self.view.bounds.width, self.view.bounds.height)
			imageView.contentMode = UIViewContentMode.Center
			
			self.tableView.backgroundView = imageView;
			return 0;
		}
		
	}
	// MARK: - CMSConnectionDelegate
	
	override func updateLocations() {
		if isUpdating || isSearching {
			appDelegate.cmsConnection!.cancelRequests()
			isSearching = false
		}
		
		if (searchBar.text == "") {
			setAllLocations([])
		}
            /*ajith error
		else {
			submittedSearchText = searchBar.text
            updatingIndicator.startAnimating()
			isSearching = true
			if (appDelegate.currentLocation != nil) {
				var coordinate = appDelegate.currentLocation!.coordinate
				appDelegate.cmsConnection!.getLocations(["q":self.searchBar.text, "lat":"\(coordinate.latitude)", "lng":"\(coordinate.longitude)"], sender: self)
			}
			else {
				appDelegate.cmsConnection!.getLocations(["q":self.searchBar.text], sender: self)
			}
		}
*/
        else {
            submittedSearchText = searchBar.text!
            updatingIndicator.startAnimating()
            isSearching = true
            if (appDelegate.currentLocation != nil) {
                var coordinate = appDelegate.currentLocation!.coordinate
                appDelegate.cmsConnection!.getLocations(["q":self.searchBar.text!, "lat":"\(coordinate.latitude)", "lng":"\(coordinate.longitude)"], sender: self)
            }
            else {
                appDelegate.cmsConnection!.getLocations(["q":self.searchBar.text!], sender: self)
            }
        }
	}
	
	override func setAllLocations(_locations: Array<Location>) {
		super.setAllLocations(_locations)
		
		if (_locations.count == 0 && isSearching) {
			//UIAlertView(title: "Search results", message: "No discounts were found that matched the search. Please try again.", delegate: nil, cancelButtonTitle: "OK").show()
			
			var noResultsLabel = UILabel(frame: CGRectMake(40.0, 0.0, self.view.bounds.width-80.0, self.view.bounds.height))
			noResultsLabel.textColor = UIColor.whiteColor()
			noResultsLabel.text = "No discounts were found that matched the search. Please try again."
			noResultsLabel.textAlignment = NSTextAlignment.Center
			noResultsLabel.numberOfLines = 2
			self.tableView.backgroundView = noResultsLabel;

		}
		else if (_locations.count == 0) {
			var imageView = UIImageView(image: UIImage(named: "search-placeholder"))
			imageView.frame = CGRectMake(0.0, 0.0, self.view.bounds.width, self.view.bounds.height)
			imageView.contentMode = UIViewContentMode.Center
			self.tableView.backgroundView = imageView;
		}
		else {
			self.tableView.backgroundView = nil
		}
		
		isSearching = false
	}
    
    /*
	
	override func updateMoreLocations() {
		updatingIndicator.startAnimating()
		if (appDelegate.currentLocation != nil) {
			var coordinate = appDelegate.currentLocation!.coordinate
			appDelegate.cmsConnection!.getMoreLocations(["q":self.searchBar.text, "lat":"\(coordinate.latitude)", "lng":"\(coordinate.longitude)", "page":"\(pagesLoaded)"], sender: self)
		}
		else {
			appDelegate.cmsConnection!.getMoreLocations(["q":self.searchBar.text, "page":"\(pagesLoaded)"], sender: self)
		}
		isUpdating = true
	}
	*/
    
    override func updateMoreLocations() {
        updatingIndicator.startAnimating()
        if (appDelegate.currentLocation != nil) {
            var coordinate = appDelegate.currentLocation!.coordinate
            appDelegate.cmsConnection!.getMoreLocations(["q":self.searchBar.text!, "lat":"\(coordinate.latitude)", "lng":"\(coordinate.longitude)", "page":"\(pagesLoaded)"], sender: self)
        }
        else {
            appDelegate.cmsConnection!.getMoreLocations(["q":self.searchBar.text!, "page":"\(pagesLoaded)"], sender: self)
        }
        isUpdating = true
    }

	// MARK: - Search Bar
	
	func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
		searchBar.setShowsCancelButton(true, animated: true)
		return true
	}
	
	func searchBarShouldEndEditing(searchBar: UISearchBar) -> Bool {
		searchBar.setShowsCancelButton(false, animated: true)
		return true
	}
	
	func searchBarCancelButtonClicked(searchBar: UISearchBar) {
		searchBar.resignFirstResponder()
	}
	
	func searchBarSearchButtonClicked(searchBar: UISearchBar) {
		if (locations.count == 0) {
			self.updateLocations()
		}
		searchBar.resignFirstResponder()
	}
	
	func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
//		if (foundNoResults && strlen(searchText) >= strlen(foundNoResultsString) && strlen(searchText) > 0) {
//			
//		}
//		else {
//			foundNoResults = false
			self.updateLocations()
//		}
	}



}
