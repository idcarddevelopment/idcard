//
//  DiscountsViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 9/22/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class DiscountsViewController: UIViewController {

	var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
	var selectedLocation: Location?
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
		applyStyles(false)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "setBadge", name: "AlertUpdateNotification", object: nil)
        setBadge()

    }

    func setBadge(){
        let count:String?
        if appDelegate.getUnreadAlertCount() != 0{
            count = "\(appDelegate.getUnreadAlertCount())"
        }else{
            count = nil
        }

        //ajith error
        //let btn = self.tabBarController?.tabBar.items?.last as? UITabBarItem
        
        let btn = self.tabBarController?.tabBar.items?.last as? UITabBarItem!
        
        btn?.badgeValue = count

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	override func viewDidAppear(animated: Bool) {
		super.viewDidAppear(animated)
		
		//Show orientation views if it's the user's first time
		var defaults = NSUserDefaults.standardUserDefaults()
		var firstLaunch = defaults.boolForKey("firstLaunch")
		if (firstLaunch) {
            var defaults = NSUserDefaults.standardUserDefaults()
            defaults.setBool(false, forKey: "firstLaunch")
            defaults.synchronize()

            self.performSegueWithIdentifier("login", sender: nil)

            //self.performSegueWithIdentifier("orientation", sender: nil)
		}
		else if (appDelegate.shouldPromptLogin && (appDelegate.cmsConnection!.username == nil || appDelegate.cmsConnection!.username == "" /*|| !appDelegate.cmsConnection!.loggedIn*/)) {
			var shouldPromptLogin = appDelegate.shouldPromptLogin as Bool
			var username = appDelegate.cmsConnection!.username! as String
			var loggedIn = appDelegate.cmsConnection!.loggedIn as Bool
			self.performSegueWithIdentifier("login", sender: nil)
		}
	}
	
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
		if (segue.destinationViewController is LocationsTableViewController) {
			var locationsViewController = segue.destinationViewController as! LocationsTableViewController
			locationsViewController.context = "location"
		}
		else if segue.destinationViewController is LocationDetailViewController {
			// from geofence notification
			var locationDetailViewController = segue.destinationViewController as! LocationDetailViewController
			if selectedLocation != nil {
				locationDetailViewController.location = selectedLocation!
			}
		}
    }

    // MARK: UICollectionViewDataSource

//    override func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
//        return 1
//    }
//
//
//    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return 2
//    }
//
//    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
//		var cell: UICollectionViewCell;
//		if (indexPath.row == 0) {
//			cell = collectionView.dequeueReusableCellWithReuseIdentifier("location", forIndexPath: indexPath) as UICollectionViewCell
//		}
//		else {
//			cell = collectionView.dequeueReusableCellWithReuseIdentifier("categories", forIndexPath: indexPath) as UICollectionViewCell
//		}
//		
//        // Configure the cell
////		var button = cell.viewWithTag(100) as UIButton
////		if indexPath.row == 0 {
////			button.setImage(UIImage(named:"location-button"), forState: UIControlState.Normal)
////		}
////		else {
////			button.setImage(UIImage(named:"categories-button"), forState: UIControlState.Normal)
////		}
//		
//		
//        return cell
//    }

    // MARK: UICollectionViewDelegate
	
//	func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
//		var flowLayout = collectionViewLayout as UICollectionViewFlowLayout
//		var itemsPerRow =  min(collectionView.numberOfItemsInSection(section), Int(collectionView.frame.size.width / flowLayout.itemSize.width))
//		var itemSpacing = (collectionView.frame.size.width - (CGFloat(itemsPerRow) * flowLayout.itemSize.width)) / (CGFloat(itemsPerRow) + 1.0);
//		var rowCount = collectionView.numberOfItemsInSection(section) / itemsPerRow
//		
//		
//		var rowSpacing = (collectionView.frame.size.height - collectionView.contentInset.bottom - collectionView.contentInset.top - (CGFloat(rowCount) * flowLayout.itemSize.height)) / (CGFloat(rowCount) + 1.0)
//		
//		return UIEdgeInsetsMake(rowSpacing, itemSpacing, rowSpacing, itemSpacing);
//	}
	
    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    func collectionView(collectionView: UICollectionView!, shouldHighlightItemAtIndexPath indexPath: NSIndexPath!) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    func collectionView(collectionView: UICollectionView!, shouldSelectItemAtIndexPath indexPath: NSIndexPath!) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    func collectionView(collectionView: UICollectionView!, shouldShowMenuForItemAtIndexPath indexPath: NSIndexPath!) -> Bool {
        return false
    }

    func collectionView(collectionView: UICollectionView!, canPerformAction action: String!, forItemAtIndexPath indexPath: NSIndexPath!, withSender sender: AnyObject!) -> Bool {
        return false
    }

    func collectionView(collectionView: UICollectionView!, performAction action: String!, forItemAtIndexPath indexPath: NSIndexPath!, withSender sender: AnyObject!) {
    
    }
    */
}
