//
//  EstablishmentCategory.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/13/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class EstablishmentCategory: NSObject {
	var name: String = ""
	var id: Int = 0
	var location_count: Int = 0
}
