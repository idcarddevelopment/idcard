//
//  AdViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/29/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit
import CoreMedia
import AVFoundation

class AdViewController: UIViewController {
	
	var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
	
	var ad: Ad?
	
	@IBOutlet weak var backgroundButton: UIButton!
	@IBOutlet weak var timerButton: UIButton!
	@IBOutlet weak var imageView: UIImageView!
	@IBOutlet weak var playerView: AdPlayerView!
	
	var duration: Int = 10
	var timeElapsed: Int = 0
	var countdownTimer: NSTimer?
	
    override func viewDidLoad() {
        super.viewDidLoad()
		backgroundButton.enabled = false
		timerButton.enabled = false
		
	}
	
	override func viewWillAppear(animated: Bool) {
		if (ad != nil) {
			if (ad!.ad_type == "image") {
                //nri error
				//var jpgRep = UIImageJPEGRepresentation(ad!.image, 1.0)
                var jpgRep = UIImageJPEGRepresentation(ad!.image!, 1.0)
				if (jpgRep == nil) { // There's a weird bug where the image appears to load, but results in an all-black image view.  This is the only way I can find to catch it.
					ad!.image = UIImage(data: NSData(contentsOfURL: NSURL(string: ad!.url)!)!)
				}
				imageView.hidden = false
				playerView.hidden = true
				self.duration = Int(ad!.duration)
				imageView.image = ad!.image
			}
			else { // video
				imageView.hidden = true
				playerView.hidden = false
				var contentURL = NSURL(string: ad!.url)
				if (contentURL != nil) {
                    //nri error
					//var playerItem = AVPlayerItem(URL: contentURL)
                    var playerItem = AVPlayerItem(URL: contentURL!)
					var player = AVPlayer(playerItem: playerItem)
					playerView.player = player
					player.play()
					backgroundButton.hidden = true
					timerButton.hidden = true
					self.duration = 0
					//self.duration = Int(CMTimeGetSeconds(playerItem.duration))
					NSNotificationCenter.defaultCenter().addObserver(self, selector: "itemDidFinishPlaying:", name: AVPlayerItemDidPlayToEndTimeNotification, object: playerItem)
				}

			}
		}
		timerButton.setTitle("\(duration)", forState: UIControlState.Disabled)
		
		//player.set
	}
	
	@IBAction func didClickAd(sender: AnyObject) {
		if ad != nil && ad!.link != nil {
			var url = NSURL(string: ad!.link!)
			if url != nil {
				UIApplication.sharedApplication().openURL(url!)
			}
		}
	}
	
	func itemDidFinishPlaying(playerItem: AVPlayerItem) {
		backgroundButton.hidden = false
		backgroundButton.enabled = true
		timerButton.hidden = false
		timerButton.enabled = true
		timerButton.setTitle("", forState: UIControlState.Normal)
	}
	
	override func viewDidAppear(animated: Bool) {
		if (ad!.ad_type == "image") {
			countdownTimer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: "timerDidCountDown", userInfo: nil, repeats: true)
		}
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	//nri error
	/*override func supportedInterfaceOrientations() -> Int
		if (ad != nil && ad!.ad_type == "video") {
			return Int(UIInterfaceOrientationMask.Landscape.rawValue)
		}
		else {
			return Int(UIInterfaceOrientationMask.Portrait.rawValue)
		}
	}
    override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
        return UIInterfaceOrientation.Portrait
    }*/  //error
    
    
    override func supportedInterfaceOrientations() -> UIInterfaceOrientationMask
    {
    if (ad != nil && ad!.ad_type == "video") {
    return UIInterfaceOrientationMask.Landscape
    }
    else {
    return UIInterfaceOrientationMask.Portrait
    }
}
override func preferredInterfaceOrientationForPresentation() -> UIInterfaceOrientation {
    return UIInterfaceOrientation.Portrait
}

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
	
	func pauseVideo() {
		if (playerView.player != nil) {
			playerView.player!.pause()
		}
	}
	
	func playVideo() {
		if (playerView.player != nil) {
			playerView.player!.play()
		}
	}
	
	func timerDidCountDown() {
		timeElapsed++
		if (timeElapsed >= duration) {
			countdownTimer!.invalidate()
			backgroundButton.enabled = true
			timerButton.enabled = true
			timerButton.setTitle("", forState: UIControlState.Normal)
		}
		else {
			timerButton.setTitle("\(duration - timeElapsed)", forState: UIControlState.Disabled)
		}
	}
	
	@IBAction func dismiss(sender: AnyObject) {
		self.dismissViewControllerAnimated(true,
			completion: {
				self.appDelegate.scheduleNextAdTimer()
			}
		)
	}
}
