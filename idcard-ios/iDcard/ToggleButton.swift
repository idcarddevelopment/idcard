//
//  ToggleButton.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/20/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//
//vishnu test
import UIKit

class ToggleButton: UIButton {
	
	override var enabled: Bool {
		didSet {
			self.layer.opacity = enabled ? 1.0 : 0.5
		}
	}
	
	override var selected: Bool {
		didSet {
			if (selected) {
				self.layer.backgroundColor = UIColor.highlightColor().CGColor
			}
			else {
				self.layer.backgroundColor = UIColor.blackColor().CGColor
			}
		}
	}
	
	override var highlighted: Bool {
		didSet {
			if (highlighted) {
				self.layer.backgroundColor = UIColor.highlightColor().CGColor
			}
			else {
				self.layer.backgroundColor = UIColor.blackColor().CGColor
			}
		}
	}
	
	required init(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)!
		self.layer.borderWidth = 2.0;
		self.layer.borderColor = UIColor.highlightColor().CGColor
	}
	
	override func layoutSubviews() {
		self.layer.cornerRadius = min(self.bounds.size.width, self.bounds.size.height)/2.0
		super.layoutSubviews()
		
		//make sure the right background color is drawn initially
		self.highlighted = self.highlighted.boolValue
		self.selected = self.selected.boolValue
		self.enabled = self.enabled.boolValue
	}
	
}
