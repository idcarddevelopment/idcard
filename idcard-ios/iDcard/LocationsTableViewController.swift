//
//  LocationsTableViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 9/24/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit
import MapKit

class LocationsTableViewController: UITableViewController, MKMapViewDelegate, CMSConnectionDelegate, UIAlertViewDelegate {
	
	var locations: Array<Location> = []
	var context = "location" // "category"
	var pagesLoaded = 0
	let pageSize = 20
	var category: String = "All"//{ didSet { if (categoryLabel != nil) { categoryLabel.text = category } } }
	
	var isUpdating = false;
	var isShowingMapView = false
	var isShowingDetail = false //to prevent a stupid bug where table view cells can be triggered twice, causing two segues to be performed
	
	var willUpdateMore = false
	
	@IBOutlet weak var updatingIndicator: UIActivityIndicatorView!
	@IBOutlet weak var categoryHeader: UIView!
	@IBOutlet weak var editCategoryButton: UIButton!
	@IBOutlet weak var regionButton: UIButton!
	@IBOutlet weak var titleLabel: UILabel!
	
	var appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
	
	var mapButton: UIBarButtonItem?
	var listButton: UIBarButtonItem?
	
	var mapView = MKMapView()
	var mapRefreshButton = IDCButton()
	
	var locationNotFoundAlert: UIAlertView?
	
	var hasShownButton = false // to keep the button from showing then disappearing when we first open the view
	var lastUpdate = NSDate()

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
		
		self.refreshControl = UIRefreshControl()
        refreshControl!.tintColor = UIColor.whiteColor()
		self.refreshControl!.addTarget(self, action: Selector("updateLocations"), forControlEvents: UIControlEvents.ValueChanged)
		
		mapButton = UIBarButtonItem(image: UIImage(named: "map-toggle-button"), style: UIBarButtonItemStyle.Plain, target: self, action: "showMapViewAnimated")
		listButton = UIBarButtonItem(image: UIImage(named: "list-toggle-button"), style: UIBarButtonItemStyle.Plain, target: self, action: "showListView")
		
		//mapView = NSBundle.mainBundle().loadNibNamed("MapView", owner: self, options: nil)[0] as? UIView
		//mapView = //MKMapView(frame: self.view.frame)
		mapView.delegate = self
		mapView.showsUserLocation = true
		mapView.userTrackingMode = MKUserTrackingMode.Follow
		mapView.rotateEnabled = false
		
		mapRefreshButton.setTitle("Reload here".localized, forState: UIControlState.Normal)
		mapRefreshButton.backgroundColor = UIColor.whiteColor()
		mapRefreshButton.addTarget(self, action: "updateLocationsFromMapCenter:", forControlEvents: UIControlEvents.TouchUpInside)
		mapRefreshButton.setTitleColor(UIColor.highlightColor(), forState: UIControlState.Highlighted)

		applyStyles(true)
		
		locationNotFoundAlert = UIAlertView(title: "Location Error".localized, message: "Your location could not be determined at this time.  Please make sure location services are enabled and try again.".localized, delegate: self, cancelButtonTitle: "OK")
		
		NSNotificationCenter.defaultCenter().addObserver(self, selector: "applicationDidEnterForeground:", name: UIApplicationWillEnterForegroundNotification, object: nil)

    }
	
	func applicationDidEnterForeground(sender: AnyObject?) {
		if lastUpdate.timeIntervalSinceNow < -900.0 { // Refresh if hasn't been updated in 15 minutes
			updateLocations()
		}
	}
	
	deinit {
		NSNotificationCenter.defaultCenter().removeObserver(self)
	}


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		if (context == "location") {
			self.navigationItem.setRightBarButtonItem((isShowingMapView ? listButton : mapButton), animated: false)
			//self.title = "Discounts Near Me"
			if (self.titleLabel != nil) {
				self.titleLabel.text = "Discounts Near Me".localized
			}
			
			if (isShowingMapView) {
				mapView.frame = self.tableView.frame
				var frame = mapView.frame
				if (mapView.superview != nil) {
					mapView.removeFromSuperview()
				}
				self.tableView.addSubview(mapView)
			}
			
			if (categoryHeader != nil) {
				var headerFrame = categoryHeader.frame
				headerFrame.size.height = 73.0
				categoryHeader.frame = headerFrame
				self.tableView.tableHeaderView = categoryHeader
			}
		}
		else {
			//self.title = self.category
			self.titleLabel.text = self.category
			
			if (categoryHeader != nil) {
				var headerFrame = categoryHeader.frame
				headerFrame.size.height = 38.0
				categoryHeader.frame = headerFrame
				self.tableView.tableHeaderView = categoryHeader
			}
		}
		
		if (regionButton != nil) {
			regionButton.setTitle(appDelegate.currentRegion, forState: UIControlState.Normal)
		}
		
	}
	
	override func viewDidAppear(animated: Bool) {
		super.viewDidAppear(animated)
		
		if locations.count == 0 || lastUpdate.timeIntervalSinceNow < -900.0 {
			updateLocations()
		}
		else {
			tableView.reloadData()
		}
		
		if (isShowingMapView) {
			showMapView(false)
		}
		
		isShowingDetail = false
		
	}
	
	func showMapViewAnimated() {
		showMapView(true)
	}
	func showMapView(animated: Bool) {
		
		isShowingMapView = true
		mapView.frame = self.tableView.frame
		var frame = mapView.frame
		if (mapView.superview != nil) {
			mapView.removeFromSuperview()
		}
		self.tableView.superview!.insertSubview(mapView, aboveSubview: tableView)
		self.tableView.superview!.insertSubview(mapRefreshButton, aboveSubview: mapView)
		
		hasShownButton = false
		mapRefreshButton.frame = CGRectMake(mapView.frame.size.width / 2.0 - 80.0, mapView.frame.size.height - 100.0, 160.0, 44.0)
		mapRefreshButton.hidden = true
		mapRefreshButton.alpha = 0.0
		
		if (animated) {
			mapView.alpha = 0.0
			mapRefreshButton.alpha = 0.0
			mapRefreshButton.hidden = true
			
			UIView.animateWithDuration(0.5,
				animations: { () in
					self.mapView.alpha = 1.0
					self.mapRefreshButton.alpha = 0.0
				},
				completion: { (finished) in
					self.navigationItem.setRightBarButtonItem(self.listButton, animated: true)
					self.mapView.alpha = 1.0
					self.mapRefreshButton.alpha = 0.0
					self.mapRefreshButton.hidden = true
					self.hasShownButton = true
				}
			)
		}
		
		if (appDelegate.currentLocation != nil) {
//			if (locations.count > 0) {
//				var furthestLocation = locations.last
//				//var radius = furthestLocation!.distance * 1609.34 * 2.0 * 1.2 // miles to meters, double for radius, add a 20% buffer
//				var region = mapView.regionThatFits(MKCoordinateRegionMakeWithDistance(appDelegate.currentLocation!.coordinate, radius, radius))
//				mapView.setRegion(region, animated: false)
//			}
//			else {
//				var region = mapView.regionThatFits(MKCoordinateRegionMakeWithDistance(appDelegate.currentLocation!.coordinate, 30000.0, 30000.0))
//				mapView.setRegion(region, animated: false)
//			}
			
			var region = mapView.regionThatFits(MKCoordinateRegionMakeWithDistance(appDelegate.currentLocation!.coordinate, 1000.0, 1000.0))
			mapView.setRegion(region, animated: false)
		}
		
		
		
	}
	
	func showListView() {
		isShowingMapView = false
		UIView.animateWithDuration(0.5,
			animations: { () in
				self.mapView.alpha = 0.0
				self.mapRefreshButton.alpha = 0.0
			},
			completion: { (finished) in
				self.navigationItem.setRightBarButtonItem(self.mapButton, animated: true)
				self.mapView.removeFromSuperview()
				self.mapRefreshButton.removeFromSuperview()
			}
		)
	}
	
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView?) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView?, numberOfRowsInSection section: Int) -> Int {
        return self.locations.count
    }

    override func tableView(tableView: UITableView?, cellForRowAtIndexPath indexPath: NSIndexPath?) -> UITableViewCell {
        let cell = tableView!.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath!) as! UITableViewCell
		
		var imageView = cell.viewWithTag(100) as! UIImageView
		var titleLabel = cell.viewWithTag(101) as! UILabel
		var distanceLabel = cell.viewWithTag(102) as! UILabel
		var categoriesLabel = cell.viewWithTag(103) as! UILabel
		
		var location = self.locations[indexPath!.row] as Location
		if (appDelegate.currentLocation != nil) {
			location.distance = self.appDelegate.currentLocation!.distanceFromLocation(location.clLocation()) * 0.000621371 // meters to miles
		}
		
		titleLabel.text = location.name
		titleLabel.sizeToFit()
		
		if (location.logo != nil) {
			imageView.image = location.logo!
		}
		else {
			imageView.image = nil
			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
				var newLocation = location
				var newLogo = UIImage(data: NSData(contentsOfURL: NSURL(string: location.logo_url)!)!)
				dispatch_async(dispatch_get_main_queue()) {
					newLocation.logo = newLogo
					var visibleCell = tableView!.cellForRowAtIndexPath(indexPath!)
					if (visibleCell != nil) {
						imageView.image = newLogo
					}
				}
			}
		}
		imageView.layer.minificationFilter = kCAFilterTrilinear
		
		if (appDelegate.currentLocation != nil) {
			distanceLabel.text = location.formattedDistance()
		}
		else {
			distanceLabel.text = ""
		}
		categoriesLabel.text = location.categories.combine(", ")
		
		var regularAttributes = [NSFontAttributeName : UIFont.systemFontOfSize(14.0)]
		var boldAttributes = [NSFontAttributeName : UIFont.boldSystemFontOfSize(14.0)]
		
		var selectedView = UIView()
		selectedView.backgroundColor = UIColor.highlightColor()
		cell.selectedBackgroundView = selectedView

		return cell
	}
	
	override func tableView(tableView: UITableView?, didSelectRowAtIndexPath indexPath: NSIndexPath?) {
		if (!isUpdating && !isShowingDetail) {
			isShowingDetail = true
			self.performSegueWithIdentifier("detail", sender: self)
		}
	}
	
	override func scrollViewDidScroll(scrollView: UIScrollView) {
		if (context == "location" || context == "category") {
			if (scrollView == self.tableView) {
				var height = scrollView.frame.size.height
				var contentOffsetY = scrollView.contentOffset.y
				var distanceFromBottom = scrollView.contentSize.height - contentOffsetY
				
				var locCount = locations.count
				if (distanceFromBottom <= height && !isUpdating && locCount >= pagesLoaded * pageSize && locCount > 0) {
					willUpdateMore = true
					self.tableView.scrollRectToVisible(CGRectMake(0.0, scrollView.contentSize.height, 1.0, 1.0), animated: true)
				}
			}
		}
	}

	override func scrollViewDidEndScrollingAnimation(scrollView: UIScrollView) {
		if (willUpdateMore) {
			willUpdateMore = false
			self.updateMoreLocations()
		}
	}
	


    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView!, moveRowAtIndexPath fromIndexPath: NSIndexPath!, toIndexPath: NSIndexPath!) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */


	// MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
		/*ajith error
if (segue.destinationViewController is LocationDetailViewController) {
			var detailViewController = segue.destinationViewController as! LocationDetailViewController
			var selectedPath = self.tableView.indexPathForSelectedRow()!
			detailViewController.location = self.locations[selectedPath.row]
		}
*/
        if (segue.destinationViewController is LocationDetailViewController) {
            var detailViewController = segue.destinationViewController as! LocationDetailViewController
            var selectedPath = self.tableView.indexPathForSelectedRow!
            detailViewController.location = self.locations[selectedPath.row]
        }
		else if (segue.destinationViewController is CategoriesPopoverViewController) {
			var detailViewController = segue.destinationViewController as! CategoriesPopoverViewController
			detailViewController.locationsTableViewController = self
		}
		else if (segue.destinationViewController is RegionPickerViewController) {
			var destinationViewController = segue.destinationViewController as! RegionPickerViewController
			destinationViewController.locationsTableViewController = self
		}
    }

	
	// MARK: - CMSConnectionDelegate
	
	func setCMSError(error: NSError) {
		UIAlertView(title: "Error".localized, message: error.localizedDescription, delegate: self, cancelButtonTitle: "OK").show()
	}
	
	func updateLocations() {
		self.locations = []
		pagesLoaded = 0
		tableView.reloadData()
        updatingIndicator.startAnimating()
        if (context == "location") {
			if (appDelegate.currentLocation != nil) {
				var coordinate = appDelegate.currentLocation!.coordinate
				isUpdating = true
				//self.tableView.userInteractionEnabled = false // prevent user from potentially bringing up a blank detail view
				appDelegate.cmsConnection!.getLocations(["lat":"\(coordinate.latitude)", "lng":"\(coordinate.longitude)", "category":category, "region":appDelegate.currentRegion!], sender: self)
			}
			else {
				locationNotFoundAlert!.show()
				updatingIndicator.stopAnimating()
			}
		}
		else if (context == "category") {
            var region = (appDelegate.currentRegion == nil) ? "All" : appDelegate.currentRegion!
			isUpdating = true
			if (appDelegate.currentLocation == nil) {
				appDelegate.cmsConnection!.getLocations(["category":category, "region":region], sender: self)
			}
			else {
				var coordinate:CLLocationCoordinate2D = appDelegate.currentLocation!.coordinate
				appDelegate.cmsConnection!.getLocations(["lat":"\(coordinate.latitude)", "lng":"\(coordinate.longitude)", "category":category, "region":region], sender: self)
			}
			//self.tableView.userInteractionEnabled = false
		}
	}
	
	func updateLocationsFromMapCenter(sender: AnyObject?) {
		self.locations = []
		pagesLoaded = 0
		tableView.reloadData()
		updatingIndicator.startAnimating()
		isUpdating = true
		//self.tableView.userInteractionEnabled = false // prevent user from potentially bringing up a blank detail view
		appDelegate.cmsConnection!.getLocations(["lat":"\(mapView.centerCoordinate.latitude)", "lng":"\(mapView.centerCoordinate.longitude)", "category":category, "region":appDelegate.currentRegion!], sender: self)
		mapRefreshButton.enabled = false
	}

	func setAllLocations(_locations: Array<Location>) {
		lastUpdate = NSDate()
		self.locations = _locations
		self.refreshControl!.endRefreshing()
        updatingIndicator.stopAnimating()
        self.tableView.reloadData()
		
		pagesLoaded = 1
		
		mapView.removeAnnotations(mapView.annotations)
		mapView.addAnnotations(self.mapAnnotations())
		
		isUpdating = false
		self.tableView.userInteractionEnabled = true
		mapRefreshButton.hidden = true
	}
	
	func updateMoreLocations() {
		updatingIndicator.startAnimating()
		if (context == "location") {
			if (appDelegate.currentLocation != nil) {
				var coordinate = appDelegate.currentLocation!.coordinate
				isUpdating = true
				appDelegate.cmsConnection!.getMoreLocations(["lat":"\(coordinate.latitude)", "lng":"\(coordinate.longitude)", "category":category, "region":appDelegate.currentRegion!, "page":"\(pagesLoaded)"], sender: self)
			}
			else {
				locationNotFoundAlert!.show()
				updatingIndicator.stopAnimating()
			}
		}
		else if (context == "category") {
			var region = (appDelegate.currentRegion == nil) ? "All" : appDelegate.currentRegion!
			isUpdating = true
			if (appDelegate.currentLocation != nil) {
				var coordinate = appDelegate.currentLocation!.coordinate
				appDelegate.cmsConnection!.getMoreLocations(["lat":"\(coordinate.latitude)", "lng":"\(coordinate.longitude)", "category":category, "region":region, "page":"\(pagesLoaded)"], sender: self)
			}
			else {
				appDelegate.cmsConnection!.getMoreLocations(["category":category, "region":region, "page":"\(pagesLoaded)"], sender: self)
			}
		}
		else {
			updatingIndicator.stopAnimating()
		}
	}
	
	func addLocations(_locations: [Location]) {
		self.locations += _locations
		self.refreshControl!.endRefreshing()
		updatingIndicator.stopAnimating()
		self.tableView.reloadData()
		
		self.pagesLoaded += 1
		
		mapView.removeAnnotations(mapView.annotations)
		mapView.addAnnotations(self.mapAnnotations())
		
		isUpdating = false
		self.tableView.userInteractionEnabled = true
	}
	
	// MARK: - Map View
	
	func mapAnnotations() -> [MapAnnotation] {
		var annotations: [MapAnnotation] = []
		for location in self.locations {
			var annotation = MapAnnotation()
			annotation.location = location
			annotations += [annotation]
		}
		return annotations
	}
	
	func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
		
		if (annotation is MKUserLocation) {
			return nil
		}
		else {
			var annotationView = mapView.dequeueReusableAnnotationViewWithIdentifier("annotationView")
			/*ajith error
if (annotationView == nil) {
annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "annotationView")
annotationView.canShowCallout = true
annotationView.rightCalloutAccessoryView = UIButton.buttonWithType(UIButtonType.DetailDisclosure) as! UIView
annotationView.centerOffset = CGPointMake(0.0, -20.0)
annotationView.image = UIImage(named: "map-annotation")
}


*/
			if (annotationView == nil) {
				annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: "annotationView")
				annotationView!.canShowCallout = true
				annotationView!.rightCalloutAccessoryView = UIButton(type: UIButtonType.DetailDisclosure) as UIView
				annotationView!.centerOffset = CGPointMake(0.0, -20.0)
				annotationView!.image = UIImage(named: "map-annotation")
			}
			return annotationView
		}
		
	}
	
	func mapView(mapView: MKMapView!, annotationView view: MKAnnotationView!, calloutAccessoryControlTapped control: UIControl!) {
		var mapAnnotation = view.annotation as! MapAnnotation
        /*ajith error
		var index = find(self.locations, mapAnnotation.location)!
*/
        let index = self.locations.indexOf(mapAnnotation.location) // find(self.locations, mapAnnotation.location)!
        /*ajith error
		self.tableView.selectRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0), animated: false, scrollPosition: UITableViewScrollPosition.None)*/
        
        self.tableView.selectRowAtIndexPath(NSIndexPath(forRow: index!, inSection: 0), animated: false, scrollPosition: UITableViewScrollPosition.None)
        
		self.performSegueWithIdentifier("detail", sender: self)
	}
	
	func mapView(mapView: MKMapView!, regionDidChangeAnimated animated: Bool) {
		if (hasShownButton) {
			mapRefreshButton.hidden = false
			mapRefreshButton.enabled = true
		}
		hasShownButton = true
	}
	
	func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
		if (alertView == locationNotFoundAlert) {
			self.navigationController!.popViewControllerAnimated(true)
		}
        
	}
	
	
	
	

}
