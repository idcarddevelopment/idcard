//
//  AdPlayerView.swift
//  iDcard
//
//  Created by Philip Kirkham on 11/26/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit
import AVFoundation

class AdPlayerView: UIView {
	
	var player: AVPlayer? {
		set {
			var playerLayer = self.layer as! AVPlayerLayer
			playerLayer.player = newValue
		}
		get {
			var playerLayer = self.layer as! AVPlayerLayer
			return playerLayer.player
		}
	}

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
	
	override class func layerClass() -> AnyClass {
		return AVPlayerLayer.self
	}
	
//	- (id)initWithFrame:(CGRect)frame
//	{
//	self = [super initWithFrame:frame];
//	if (self) {
//	// Initialization code
//	}
//	return self;
//	}
//	
//	+ (Class)layerClass {
//	return [AVPlayerLayer class];
//	}
//	- (AVPlayer*)player {
//	return [(AVPlayerLayer *)[self layer] player];
//	}
//	- (void)setPlayer:(AVPlayer *)player {
//	[(AVPlayerLayer *)[self layer] setPlayer:player];
//	}


}
