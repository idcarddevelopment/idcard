//
//  Location.swift
//  iDcard
//
//  Created by Philip Kirkham on 9/18/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit
import CoreLocation

class Location: NSObject {
	
	var name: String = ""
	var id: Int = 0
	var street_address = ""
	var secondary_address = ""
	var city = ""
	var state = ""
	var country = ""
	var postal_code = ""
	var phone: String = ""
	var latitude: Double = 0.0
	var longitude: Double = 0.0
	var distance: Double = 0.0
	var website: String = ""
	var categories: Array<String> = []
	var logo_url: String = "" //{ didSet { setLogoFromURL() } }
	var logo: UIImage?
	var banner_url: String = ""
	var banner: UIImage?
	var favorite: Bool = false
	var discount: String?
	var disclaimer:String?
	var can_check_in:Bool = false

	func setLogoFromURL() {
		if (logo_url != "") {
			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
				var newLogo = UIImage(data: NSData(contentsOfURL: NSURL(string: self.logo_url)!)!)
				
				dispatch_async(dispatch_get_main_queue()) {
					self.logo = newLogo
				}
			}
		}
	}
	
	func formattedDistance() -> String {
		var distanceUnits = NSUserDefaults.standardUserDefaults().stringForKey("distanceUnits")
        let miles = "miles".localized
		if (distanceUnits == "mi") {
			return String(format: (distance <= 10 ? "%.1f \(miles)" : "%.f \(miles)"), distance)
		}
		else {
			var kmDistance = distance * 1.60934
			return String(format: (kmDistance <= 10 ? "%.1f km" : "%.f km"), kmDistance)
		}
	}
	
	func phoneURL() -> NSURL? {
		if phone == "" {
			return nil
		}
		else {
			return NSURL(string: "tel:\(phone)")
		}
	}
	
	func websiteURL() -> NSURL? {
		return NSURL(string: website)
	}
	
	func clLocation() -> CLLocation {
		return CLLocation(latitude: latitude, longitude: longitude)
	}

}
