//
//  OrientationLabel.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/2/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class OrientationLabel: UIView {
	
	var backgroundImage = UIImage(named: "orientation-label-background")!.resizableImageWithCapInsets(UIEdgeInsetsMake(20.0, 6.0, 6.0, 6.0))
	var pointerImage = UIImage(named: "orientation-label-pointer")

    override func drawRect(rect: CGRect)
    {
		backgroundImage.drawInRect(self.bounds)
		pointerImage!.drawInRect(CGRectMake(self.bounds.width/2.0-8.0, 0.0, 16.0, 16.0))
		
    }

}
