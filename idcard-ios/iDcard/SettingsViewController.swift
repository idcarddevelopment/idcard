//
//  SettingsViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/20/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

	var defaults = NSUserDefaults.standardUserDefaults()
	var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

	@IBOutlet weak var milesButton: UIButton!
	@IBOutlet weak var kilometersButton: UIButton!
	//@IBOutlet weak var geofenceOnButton: UIButton!
	//@IBOutlet weak var geofenceOffButton: UIButton!
	@IBOutlet weak var distanceBackgroundView: UIImageView!
	//@IBOutlet weak var geofenceBackgroundView: UIImageView!
	
    override func viewDidLoad() {
        super.viewDidLoad()
		applyStyles(true)
		
		var buttonImage = UIImage(named: "toggle-button-highlighted")!.resizableImageWithCapInsets(UIEdgeInsetsMake(24.0, 24.0, 24.0, 24.0))
		var backgroundImage = UIImage(named: "toggle-button-background")!.resizableImageWithCapInsets(UIEdgeInsetsMake(24.0, 24.0, 24.0, 24.0))
		
		distanceBackgroundView.image = backgroundImage
//		geofenceBackgroundView.image = backgroundImage
		
		milesButton.setBackgroundImage(buttonImage, forState: UIControlState.Selected)
		kilometersButton.setBackgroundImage(buttonImage, forState: UIControlState.Selected)
//		geofenceOnButton.setBackgroundImage(buttonImage, forState: UIControlState.Selected)
//		geofenceOffButton.setBackgroundImage(buttonImage, forState: UIControlState.Selected)
		
//		var allowGeofenceAlerts = defaults.boolForKey("allowGeofenceAlerts")
		var distanceUnits = defaults.stringForKey("distanceUnits")
		
		if (distanceUnits == "km") {
			milesButton.selected = false
			kilometersButton.selected = true
		}
		else {
			kilometersButton.selected = false
			milesButton.selected = true
		}
		
//		if (allowGeofenceAlerts) {
//			geofenceOffButton.selected = false
//			geofenceOnButton.selected = true
//		}
//		else {
//			geofenceOnButton.selected = false
//			geofenceOffButton.selected = true
//		}
		
		//self.title = "Settings"
		
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
	@IBAction func selectMiles(sender: AnyObject) {
		defaults.setValue("mi", forKey: "distanceUnits")
		defaults.synchronize()
		kilometersButton.selected = false
		milesButton.selected = true
	}
	
	@IBAction func selectKilometers(sender: AnyObject) {
		defaults.setValue("km", forKey: "distanceUnits")
		defaults.synchronize()
		kilometersButton.selected = true
		milesButton.selected = false
	}
	
//	@IBAction func selectGeofenceOn(sender: AnyObject) {
//		defaults.setBool(true, forKey: "allowGeofenceAlerts")
//		defaults.synchronize()
//		geofenceOffButton.selected = false
//		geofenceOnButton.selected = true
//
//		appDelegate.updateMonitoredRegions()
//	}
//	
//	@IBAction func selectGeofenceOff(sender: AnyObject) {
//		defaults.setBool(false, forKey: "allowGeofenceAlerts")
//		defaults.synchronize()
//		geofenceOffButton.selected = true
//		geofenceOnButton.selected = false
//		
//		appDelegate.updateMonitoredRegions()
//	}
	
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
