//
//  IDCNavigationController.swift
//  iDcard
//
//  Created by Philip Kirkham on 9/22/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class IDCNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
		
		var logoView = UIImageView(image: UIImage(named: "header-logo"))
		logoView.contentMode = UIViewContentMode.ScaleAspectFit
		logoView.frame = CGRectMake(0, 0, 132, 32)
		self.view.insertSubview(logoView, atIndex: 0)
		var navView = self.view
		self.view.opaque = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
