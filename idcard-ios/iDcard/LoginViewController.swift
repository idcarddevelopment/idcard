//
//  LoginViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/20/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, CMSLoginDelegate, UITextFieldDelegate, FBLoginViewDelegate ,UIScrollViewDelegate{
	
	var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
	
	@IBOutlet weak var usernameField: UITextField!
	@IBOutlet weak var passwordField: UITextField!
	@IBOutlet weak var waitingView: UIView!
    @IBOutlet weak var logoView: UIImageView!
    @IBOutlet weak var scrolView: UIScrollView!

	
    @IBOutlet weak var keyboardBackgroundView: UIView!
    @IBOutlet weak var fbBackgroundView: UIView!
    @IBOutlet weak var keyboardBackgroundHeight: NSLayoutConstraint!
    @IBOutlet weak var logoHeight: NSLayoutConstraint!
    @IBOutlet weak var logoTop: NSLayoutConstraint!
    @IBOutlet weak var logoBottom: NSLayoutConstraint!
    @IBOutlet weak var fbBottom: NSLayoutConstraint!

    @IBOutlet weak var emailBottom: NSLayoutConstraint!
    @IBOutlet weak var pwrdBottom: NSLayoutConstraint!
    @IBOutlet weak var loginBottom: NSLayoutConstraint!
    @IBOutlet weak var regstrBottom: NSLayoutConstraint!
    @IBOutlet weak var contBottom: NSLayoutConstraint!
    @IBOutlet weak var forgotBottom: NSLayoutConstraint!
    @IBOutlet weak var fbViewWidth: NSLayoutConstraint!
    @IBOutlet weak var fbViewHeight: NSLayoutConstraint!
    @IBOutlet weak var fbViewBottom: NSLayoutConstraint!
   // @IBOutlet weak var fbBottom: NSLayoutConstraint!

    
    @IBOutlet weak var fbLoginView: FBLoginView!
    @IBOutlet weak var newUserButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var forgotButton: UIButton!
    @IBOutlet weak var noAccount: UILabel!
    
    @IBOutlet weak var loginButtonAlignment: NSLayoutConstraint!
    var optionsViewController: OptionsViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
		applyStyles(false)
        observeKeyboard()
		
        scrolView.delegate = self
        
		fbLoginView.delegate = self
		fbLoginView.readPermissions = ["public_profile", "email"]
		fbLoginView.publishPermissions = ["publish_actions"]
		fbLoginView.defaultAudience = FBSessionDefaultAudience.Everyone
//        var leftView1 : UIView = UIView(frame: CGRectMake( 0.0, 0.0, 10.0,  50.0))
//        var leftView2 : UIView = UIView(frame: CGRectMake( 0.0, 0.0, 10.0,  50.0))
//
//        usernameField.leftView = leftView1
//        passwordField.leftView = leftView2
//        usernameField.leftViewMode = UITextFieldViewMode.Always
//        passwordField.leftViewMode = UITextFieldViewMode.Always
        
        usernameField.attributedPlaceholder = NSAttributedString(string:"Email".localized,
            attributes:[NSForegroundColorAttributeName: UIColor.placeHolderColor()])
        
        passwordField.attributedPlaceholder = NSAttributedString(string:"Password".localized,
            attributes:[ NSForegroundColorAttributeName: UIColor.placeHolderColor()])
        loginButton.backgroundColor = UIColor.highlightColor()
        
        setRoundedCorners(usernameField);
        setRoundedCorners(passwordField);
        setRoundedCorners(loginButton);

        customizeFbButton()
        
        usernameField.layer.borderWidth = 2.0;
//        usernameField.layer.borderColor = UIColor.highlightColor().CGColor
        usernameField.layer.cornerRadius = min(usernameField.bounds.size.width, usernameField.bounds.size.height)/2.0

    }
    override func viewDidLayoutSubviews(){
        super.viewDidLayoutSubviews()
        configureViews()
        fbViewHeight.constant = 150
        fbViewWidth.constant = 50

    }
    func scrollViewDidScroll(scrollView: UIScrollView) {

         if (scrollView.contentOffset.x != 0) {
            var offset: CGPoint = scrollView.contentOffset
            offset.x = 0;
            scrollView.contentOffset = offset;
        }

        
    }
	override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
		waitingView.hidden = true
		
		if (appDelegate.cmsConnection!.loggedIn) {
			self.dismissViewControllerAnimated(true, completion: nil)
		}
       
        
        //logoBottom.constant = 20.0
       // keyboardBackgroundHeight.constant = 0.0
        
	}
    func configureViews(){
        
        var padding : CGFloat = CGFloat(30)
        
        var screenHeight : CGFloat = self.view.frame.size.height
        if (screenHeight == 480)
        {
            logoTop.constant = 140.0
            padding = 15
            scrolView.contentSize = CGSizeMake(self.view.frame.size.width, 780)

        }
        if (screenHeight ==  568)
        {
            logoTop.constant = 100.0

            padding = 20

            
        }
        else
        {
            padding = 30
        }
//        var consAry = [emailBottom,pwrdBottom,loginBottom,regstrBottom,contBottom,forgotBottom]
        var consAry = [emailBottom,pwrdBottom]
        
        var i : Int = Int(0)
        for constraint: NSLayoutConstraint in consAry {
            if ((i > 2) && (screenHeight <= 568)){
                padding = 10
            }
            constraint.constant = padding
            i = i + 1

        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (optionsViewController != nil) {
            optionsViewController!.viewWillAppear(true)
        }
    }
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
       //

        //fbViewWidth.constant = 120
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

	@IBAction func cancelSignIn(sender: AnyObject) {
		appDelegate.shouldPromptLogin = false
        if (usernameField.isFirstResponder()) {
            usernameField.resignFirstResponder()
        }
        else if (passwordField.isFirstResponder()) {
            passwordField.resignFirstResponder()
        }
		self.dismissViewControllerAnimated(true, completion: nil)
	}

	@IBAction func enteredUsername(sender: AnyObject) {
		passwordField.becomeFirstResponder()
	}
	
	@IBAction func enteredPassword(sender: AnyObject) {
		passwordField.becomeFirstResponder()
	}
	
	@IBAction func signIn(sender: AnyObject) {
		performSignIn()
	}

	@IBAction func createAccount(sender: AnyObject) {

	}
	
    @IBAction func forgotPasswordClicked(sender: AnyObject) {
        appDelegate.cmsConnection!.forgotPassword()
    }
    
    @IBAction func dismissKeyboard(sender: AnyObject) {
        if (usernameField.isFirstResponder()) {
            usernameField.resignFirstResponder()
        }
        else if (passwordField.isFirstResponder()) {
            passwordField.resignFirstResponder()
        }
    }
    func setRoundedCorners(view:UIView){
        view.layer.borderWidth = 2.0;
//        view.layer.borderColor = UIColor.highlightColor().CGColor
        view.layer.cornerRadius = min(view.bounds.size.width, view.bounds.size.height)/2.0

    }
    func customizeFbButton(){
        for view in fbLoginView.subviews as! [UIView] {
            if let btn = view as? UIButton {
   
                var image : UIImage = UIImage(named:"fb-icon")!
                btn.setBackgroundImage(nil, forState: UIControlState.Normal)
                btn.setBackgroundImage(nil, forState: UIControlState.Highlighted)
                btn.contentMode = UIViewContentMode.ScaleAspectFit

            }
            if let lbl = view as? UILabel {
                lbl.text = ""
            }
            
        }
        fbLoginView.sizeToFit()
    }
   
    
	func performSignIn() {
		waitingView.hidden = false;
        if (usernameField.isFirstResponder()) {
            usernameField.resignFirstResponder()
        }
        else if (passwordField.isFirstResponder()) {
            passwordField.resignFirstResponder()
        }

        var username = usernameField.text
		var password = passwordField.text
		
		var cmsConnection = appDelegate.cmsConnection
		cmsConnection!.username = username
		cmsConnection!.password = password
		
		cmsConnection!.login(self)
	}
    
	func performFacebookSignIn(fbEmail: String, fbId: String, fbName: String) {
        waitingView.hidden = false;
        if (usernameField.isFirstResponder()) {
            usernameField.resignFirstResponder()
        }
        else if (passwordField.isFirstResponder()) {
            passwordField.resignFirstResponder()
        }
        
        var cmsConnection = appDelegate.cmsConnection
        cmsConnection!.username = fbEmail
        cmsConnection!.facebook_id = fbId
        
        cmsConnection!.loginFacebook(["user[email]": fbEmail, "user[facebook_id]": fbId, "user[name]": fbName], sender: self)
    }
	
	func textFieldShouldReturn(textField: UITextField) -> Bool {
		if (textField == usernameField) {
			passwordField.becomeFirstResponder()
		}
		else if (textField == passwordField) {
			performSignIn()
		}
		return true
	}
	
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
		if (segue.destinationViewController is NewUserViewController) {
			var newUserViewController = segue.destinationViewController as! NewUserViewController
			newUserViewController.loginViewController = self
		}
    }

	func loginSucceeded() {
		self.dismissViewControllerAnimated(true, completion: nil)
		waitingView.hidden = true;
	}
	
	func loginFailed() {
		waitingView.hidden = true;
		UIAlertView(title: "Login error".localized, message: "The username and/or password you entered were not recognized.  Please try again.".localized, delegate: self, cancelButtonTitle: "OK").show()
	}
    
    func keyboardWillShow(notification: NSNotification) {
        var info = notification.userInfo!
        var kbFrame = info[UIKeyboardFrameEndUserInfoKey] as! NSValue
        var animationDuration = info[UIKeyboardAnimationDurationUserInfoKey]!.doubleValue
        var keyboardFrame = kbFrame.CGRectValue()
        var height = keyboardFrame.size.height
        
    //    logoTop.constant = -66.0
        //logoBottom.constant = 28.0
       // loginButtonAlignment.constant = 0.0
       // keyboardBackgroundHeight.constant = height
        UIView.animateWithDuration(animationDuration,
            animations: { () in
                self.view.layoutIfNeeded()
//                self.fbLoginView.hidden = true
//                self.newUserButton.hidden = true
            }
        )
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        var info = notification.userInfo!
        var animationDuration = info[UIKeyboardAnimationDurationUserInfoKey]!.doubleValue
      //  logoTop.constant = 20.0
      //  logoBottom.constant = 20.0
       // loginButtonAlignment.constant = 66.0
        //keyboardBackgroundHeight.constant = 0.0
        UIView.animateWithDuration(animationDuration,
            animations: { () in
                self.view.layoutIfNeeded()
                self.fbLoginView.hidden = false
                self.newUserButton.hidden = false
            }
        )
    }
    
    func loginViewFetchedUserInfo(loginView: FBLoginView!, user: FBGraphUser!) {
        //self.dismissViewControllerAnimated(true, completion: nil)
        
        if (FBSession.activeSession().isOpen) {
            FBRequestConnection.startForMeWithCompletionHandler({ (connection: FBRequestConnection!, fbUserData: AnyObject!, error: NSError!) in
                if (error == nil) {
                    var fbEmail = fbUserData["email"] as! String?
					var fbId = fbUserData["id"] as! String?
					var fbName = fbUserData["name"] as! String?
                    if (fbEmail != nil && fbEmail != "" && fbId != nil && fbId != "") {
						self.performFacebookSignIn(fbEmail!, fbId: fbId!, fbName: fbName!)
                    }
                    else {
                        FBSession.activeSession().closeAndClearTokenInformation();
                        UIAlertView(title: "Login error".localized, message: "iDcard needs access to your email address in order to login with Facebook", delegate: nil, cancelButtonTitle: "OK").show()
                    }
                }
            })

        }
        
    }
    
    func loginViewShowingLoggedInUser(loginView: FBLoginView!) {
        //self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func loginViewShowingLoggedOutUser(loginView: FBLoginView!) {
        //self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    

	

}
