//
//  Extensions.swift
//  iDcard
//
//  Created by Philip Kirkham on 9/29/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

extension UIColor {
    class func highlightColor() -> UIColor {
        return UIColor(red: 0.56, green: 0.73, blue: 0.90, alpha: 1.0)
    }
    class func placeHolderColor() -> UIColor {
        return UIColor(red: 0.17, green: 0.43, blue: 0.69, alpha: 1.0)
    }
}

extension Array {
	func combine(separator: String) -> String{
		var str : String = ""
		/*ajith error
        for (idx, item) in enumerate(self) {
			str += "\(item)"
			if idx < self.count-1 {
				str += separator
			}
		}*/
        for (idx, item) in  self.enumerate() {
            str += "\(item)"
            if idx < self.count-1 {
                str += separator
            }
        }
		return str
	}
}

extension UIViewController {
	
	
//	func supportedInterfaceOrientations() -> Int {
//		return Int(UIInterfaceOrientationMask.Portrait.rawValue)
//	}

	func applyStyles(logo: Bool) {
		if (logo) {
			var imageView = UIImageView(image: UIImage(named: "header-logo"))
			self.navigationItem.titleView = imageView
		}
		
		if (self.isKindOfClass(UITableViewController.classForCoder())) {
			(self as! UITableViewController).tableView!.backgroundColor = UIColor(patternImage: UIImage(named: "background-texture")!)
		}
		else if (self.respondsToSelector("collectionView")) {
			if (self is UICollectionViewController) {
				var collectionViewController = self as! UICollectionViewController
				var backgroundImage = UIImage(named: "background-texture")
				var backgroundColor = UIColor(patternImage: backgroundImage!)
				collectionViewController.collectionView!.backgroundColor = backgroundColor
				
			}
		}
		else {
			self.view.backgroundColor = UIColor(patternImage: UIImage(named: "background-texture")!)
		}
	}
    
    func observeKeyboard() {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillShow:", name: UIKeyboardWillChangeFrameNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
    }

}

extension NSData {
	func hexString() -> NSString {
		var str = NSMutableString()
		let bytes = UnsafeBufferPointer<UInt8>(start: UnsafePointer(self.bytes), count:self.length)
		for byte in bytes {
			str.appendFormat("%02hhx", byte)
		}
		return str
	}
}

extension String {
	
	func isValidEmail() -> Bool {
		return NSPredicate(format: "SELF MATCHES %@", ".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*").evaluateWithObject(self)
	}
    
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: NSBundle.mainBundle(), value: "",comment: "")
    }
}