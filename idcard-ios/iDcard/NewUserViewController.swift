//
//  NewUserViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/23/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class NewUserViewController: UIViewController, CMSLoginDelegate, UITextFieldDelegate {
	
	var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
	@IBOutlet weak var nameField: UITextField!
	@IBOutlet weak var emailField: UITextField!
	@IBOutlet weak var passwordField: UITextField!
	@IBOutlet weak var passwordConfirmationField: UITextField!
    @IBOutlet weak var waitingView: UIView!
	
    @IBOutlet weak var logoTop: NSLayoutConstraint!
    @IBOutlet weak var logoBottom: NSLayoutConstraint!
    @IBOutlet weak var keyboardBackgroundHeight: NSLayoutConstraint!
    @IBOutlet weak var createButtonHeight: NSLayoutConstraint!
    
	var username: String?
	var password: String?
	
	var loginViewController: LoginViewController?
	
    override func viewDidLoad() {
        super.viewDidLoad()

		applyStyles(false)
        observeKeyboard()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        logoTop.constant = 0.0
        waitingView.hidden = true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

	@IBAction func addUser(sender: AnyObject) {
		
		var nickname = nameField.text
		username = emailField.text
		password = passwordField.text
		
		/*ajith error
        if (strlen(nickname) == 0) {
			UIAlertView(title: "Registration error", message: "Name cannot be blank", delegate: self, cancelButtonTitle: "OK").show()
			return
		}
		if (!(username!.isValidEmail())) {
			UIAlertView(title: "Registration error", message: "Must use a valid email address", delegate: self, cancelButtonTitle: "OK").show()
			return
		}
		
		if (password == passwordConfirmationField.text) {
            waitingView.hidden = false
            passwordConfirmationField.resignFirstResponder()
			appDelegate.cmsConnection!.registerUser(nameField.text, email: username!, password: password!, sender: self)
		}
		else {
			UIAlertView(title: "Registration error", message: "Passwords must match", delegate: self, cancelButtonTitle: "OK").show()
		}*/
        
        if (strlen(nickname!) == 0) {
            UIAlertView(title: "Registration error", message: "Name cannot be blank", delegate: self, cancelButtonTitle: "OK").show()
            return
        }
        if (!(username!.isValidEmail())) {
            UIAlertView(title: "Registration error", message: "Must use a valid email address", delegate: self, cancelButtonTitle: "OK").show()
            return
        }
        
        if (password == passwordConfirmationField.text) {
            waitingView.hidden = false
            passwordConfirmationField.resignFirstResponder()
            appDelegate.cmsConnection!.registerUser(nameField.text!, email: username!, password: password!, sender: self)
        }
        else {
            UIAlertView(title: "Registration error", message: "Passwords must match", delegate: self, cancelButtonTitle: "OK").show()
        }
	}
	
	@IBAction func dismiss(sender: AnyObject) {
        if (nameField.isFirstResponder()) {
            nameField.resignFirstResponder()
        }
        else if (emailField.isFirstResponder()) {
            emailField.resignFirstResponder()
        }
        else if (passwordField.isFirstResponder()) {
            passwordField.resignFirstResponder()
        }
        else if (passwordConfirmationField.isFirstResponder()) {
            passwordConfirmationField.resignFirstResponder()
        }
		self.dismissViewControllerAnimated(true, completion: nil)
	}
	
	func didRegisterUser() {
		var cmsConnection = appDelegate.cmsConnection!
		cmsConnection.username = username
		cmsConnection.password = password
		cmsConnection.login(self)
	}
	
	func userRegistrationDidFail(message: String) {
        waitingView.hidden = true
		UIAlertView(title: "Registration error", message: message, delegate: self, cancelButtonTitle: "OK").show()
	}
	
	func loginSucceeded() {
		appDelegate.cmsConnection!.loggedIn = true
		self.dismissViewControllerAnimated(true, completion: nil)
		if (loginViewController != nil) {
			loginViewController!.viewWillAppear(false)
		}
		
	}
	
	func loginFailed() {
		//This should never happen since the user just registered successfullyl
		UIAlertView(title: "Error".localized, message: "An unknown error occurred. Please try again.", delegate: self, cancelButtonTitle: "OK")
	}
	
	func textFieldShouldReturn(textField: UITextField) -> Bool {
		if (textField == nameField) {
			emailField.becomeFirstResponder()
		}
		else if (textField == emailField) {
			passwordField.becomeFirstResponder()
		}
		else if (textField == passwordField) {
			passwordConfirmationField.becomeFirstResponder()
		}
		else if (textField == passwordConfirmationField) {
			addUser(self)
		}
		return true
	}
	
    func keyboardWillShow(notification: NSNotification) {
        var info = notification.userInfo!
        var kbFrame = info[UIKeyboardFrameEndUserInfoKey] as! NSValue
        var animationDuration = info[UIKeyboardAnimationDurationUserInfoKey]!.doubleValue
        var keyboardFrame = kbFrame.CGRectValue()
        var height = keyboardFrame.size.height
        
        logoTop.constant = -86.0
        logoBottom.constant = 28.0
        keyboardBackgroundHeight.constant = height
        createButtonHeight.constant = 0.0
        UIView.animateWithDuration(animationDuration,
            animations: { () in
                self.view.layoutIfNeeded()
            }
        )
        
    }
    
    func keyboardWillHide(notification: NSNotification) {
        var info = notification.userInfo!
        var animationDuration = info[UIKeyboardAnimationDurationUserInfoKey]!.doubleValue
        
        logoTop.constant = 0.0
        logoBottom.constant = 8.0
        keyboardBackgroundHeight.constant = 0.0
        createButtonHeight.constant = 112.0
        UIView.animateWithDuration(animationDuration,
            animations: { () in
                self.view.layoutIfNeeded()
            }
        )
        
    }
    

	
}
