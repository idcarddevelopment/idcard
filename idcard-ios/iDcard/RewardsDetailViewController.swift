//
//  RewardsDetailViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/7/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class RewardsDetailViewController: UIViewController {
	
	var reward = Reward()
	var availablePoints:Int = 0
	
	@IBOutlet weak var photoView: UIImageView!
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var pointsLabel: UILabel!
	@IBOutlet weak var descriptionView: UITextView!
	@IBOutlet weak var redeemButton: IDCButton!
	
	@IBOutlet weak var bannerHeight: NSLayoutConstraint!
	
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		
		applyStyles(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	override func viewWillAppear(animated: Bool) {
		nameLabel.text = reward.name
        let pointsText = "Points".localized
		pointsLabel.text = "\(reward.points) \(pointsText)"
		descriptionView.text = reward.detail
		
		if availablePoints < reward.points {
			redeemButton.enabled = false
		}
		else {
			redeemButton.enabled = true
		}
		
		
		if (reward.banner_url != "") {
			if (reward.banner == nil) {
				dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
					var newBanner = UIImage(data: NSData(contentsOfURL: NSURL(string: self.reward.banner_url)!)!)
					dispatch_async(dispatch_get_main_queue()) {
						self.reward.banner = newBanner
						self.photoView.image = self.reward.banner
					}
				}
				
			}
			else {
				self.photoView.image = self.reward.banner
			}
		}
		else {
			photoView.image = reward.photo
		}
		
	}

	override func viewWillLayoutSubviews() {
		var height = self.view.frame.size.height
		if (height <= 480.0) {
			bannerHeight.constant = photoView.frame.size.width * (1.0 / 2.0)
		}
		else {
			bannerHeight.constant = photoView.frame.size.width * (2.0 / 3.0)
		}
		print("View height: \(height)")
	}

    // MARK: - Navigation

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
		if (segue.destinationViewController is RedeemViewController) {
			var redeemViewController = segue.destinationViewController as! RedeemViewController
			redeemViewController.reward = self.reward
		}
    }
	
	

}
