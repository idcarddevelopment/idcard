//
//  AppDelegate.swift
//  iDcard
//
//  Created by Philip Kirkham on 8/26/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit
import CoreLocation
import AddressBook
import MapKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate, CMSConnectionDelegate, CMSLoginDelegate, UITabBarControllerDelegate, FBLoginViewDelegate {
	
    var window: UIWindow?
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var currentRegion: String?
    var alerts : [Alert] = []
    var alertHistory:[Int] = []
	
	var overallMonitoredRegion: CLCircularRegion?
	
    var regions = ["Oahu" : CLLocationCoordinate2D(latitude: 21.48, longitude: -157.96),
        "Maui" : CLLocationCoordinate2D(latitude: 20.80, longitude: -156.33),
        "Kauai" : CLLocationCoordinate2D(latitude: 22.05, longitude: -159.54),
        "Big Island" : CLLocationCoordinate2D(latitude: 19.54, longitude: -155.66),
        "Nevada" : CLLocationCoordinate2D(latitude: 38.50, longitude: -117.02),
        "All" : CLLocationCoordinate2D(latitude: 36.0, longitude: -84.0),
        "New Jersey" : CLLocationCoordinate2D(latitude: 40.14, longitude: -74.73)]
    
    
    let documentsPath = NSSearchPathForDirectoriesInDomains(.CachesDirectory, .UserDomainMask, true)[0] as! NSString
    var adsFile: String
    var ads: [Ad]?
	var adIndex = 0
	var adsLastUpdated = NSDate()
	var isShowingAd = false
    
    var cmsConnection: CMSConnection?
    var shouldPromptLogin = true // Prompt the user every time the application launches
	
    var locations: [Location] = [] {
        didSet {
            updateMonitoredRegions()
        }
    }
    
    var rootViewController: UIViewController?
    
    var favoritesHaveChanged = false
    var rewardsHaveChanged = true
    
    var adTimer: NSTimer?
	var adTimerTimeLeft: NSTimeInterval = 60.0 // for pausing and restarting the timer
	var adViewController: AdViewController?
    
    let contactPhone = "8088432273"
    let contactEmail = "info@theidcard.com"
    

    //nri error 1
    let contactMapItem = MKMapItem(placemark:MKPlacemark(coordinate: CLLocationCoordinate2DMake(21.2945791,-157.8559432), addressDictionary: [String(kABPersonAddressStreetKey) : "1050 Ala Moana Blvd, Suite 2500", String(kABPersonAddressCityKey) : "Honolulu", String(kABPersonAddressStateKey) : "HI", String(kABPersonAddressZIPKey) : "96814", String(kABPersonAddressCountryCodeKey) : "US"]) )
    
    
    
    
    let contactFacebookId = "105375456178977"
    let contactTwitterId = "theidcard"
    let contactYoutubeId = "UCxNr4BTHvfNqgXhSXIN7KHw"
    let contactInstagramId = "theidcard"
    var timer:NSTimer?
    override init() {
        adsFile = self.documentsPath.stringByAppendingPathComponent("ads.plist")
        super.init()
    }
	
	
	func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject : AnyObject]?) -> Bool {
		Flurry.startSession("RJ5WPMQT6DK6X6SXN9BW")
        var initialDefaults = NSDictionary(contentsOfFile: NSBundle.mainBundle().pathForResource("UserDefaults", ofType: "plist")!)
        //nri error 2
        //NSUserDefaults.standardUserDefaults().registerDefaults(initialDefaults! as [NSObject : AnyObject])
        NSUserDefaults.standardUserDefaults().registerDefaults(initialDefaults! as! [String : AnyObject])
        
        locationManager.delegate = self
        //nri error 3
        //if (locationManager.respondsToSelector("requestAlwaysAuthorization")) {
        //    locationManager.requestAlwaysAuthorization()
        //}
        if (locationManager.respondsToSelector("requestAlwaysAuthorization")) {
            if #available(iOS 8.0, *) {
                locationManager.requestAlwaysAuthorization()
            } else {
                // Fallback on earlier versions
            }
        }
        locationManager.distanceFilter = kCLLocationAccuracyHundredMeters
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.startUpdatingLocation()
		
        //nri error 4
        /*
        if (application.respondsToSelector(Selector("registerUserNotificationSettings:"))) {
            let settings = UIUserNotificationSettings(forTypes: UIUserNotificationType.Badge | UIUserNotificationType.Sound | UIUserNotificationType.Alert, categories: nil)
            UIApplication.sharedApplication().registerUserNotificationSettings(settings)
        } else {
            UIApplication.sharedApplication().registerForRemoteNotificationTypes(UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound | UIRemoteNotificationType.Alert)
        }*/
        
        
        if (application.respondsToSelector(Selector("registerUserNotificationSettings:"))) {
            if #available(iOS 8.0, *) {
                let settings = UIUserNotificationSettings(forTypes: [.Badge, .Sound, .Alert ], categories: nil)
                UIApplication.sharedApplication().registerUserNotificationSettings(settings)

                } else {
                // Fallback on earlier versions
                }
        } else {
            
            UIApplication.sharedApplication().registerForRemoteNotificationTypes([.Alert, .Badge, .Sound])
        }
        
        
        var defaults = NSUserDefaults.standardUserDefaults()
        var username = defaults.valueForKey("username") as! String
        var password = defaults.valueForKey("password") as! String
        cmsConnection = CMSConnection(username: username, password: password)
        cmsConnection!.delegate = self
		
		if (FBSession.activeSession().state == FBSessionState.CreatedTokenLoaded) {
			shouldPromptLogin = false
			var permissions = ["public_profile", "email"] as [AnyObject]
			FBSession.openActiveSessionWithReadPermissions(permissions, allowLoginUI: false, completionHandler: {(session: FBSession!, state: FBSessionState, error: NSError?) in
				var appDelegate = UIApplication.sharedApplication().delegate! as! AppDelegate // workaround for some weird bug that won't let me call self
				if (error == nil) {
					appDelegate.facebookSessionDidOpen()
				}
				else {
					FBSession.activeSession().closeAndClearTokenInformation();
					appDelegate.shouldPromptLogin = true
				}
			})
		}
		else {
			cmsConnection!.login(self)
		}
//		if (FBSession.activeSession().isOpen) {
//			
////			[FBRequestConnection startForMeWithCompletionHandler:^(FBRequestConnection *connection,
////				id<FBGraphUser> fbUserData,
////				NSError *error)
////    {
////				
////				if (!error)
////				{
////				
////				if ([fbUserData objectForKey:@"email"])
////				{
////				NSString *FBEmail = [fbUserData objectForKey:@"email"];
////				
////				if (![FBEmail isEqualToString:@""] && FBEmail != nil)
////				{
//
//			//cmsConnection!.loginFacebook([], sender: self)
//		}
//		else {
//			cmsConnection!.login(self)
//		}
		
				
        UITabBar.appearance().tintColor = UIColor.highlightColor()
        var tabBarController = self.window!.rootViewController as! UITabBarController
        tabBarController.selectedIndex = 2
        
        tabBarController.delegate = self
        
        rootViewController = self.window!.rootViewController
        
        //Download the latest ads
        self.ads = [] //savedAds()
        self.updateAds()
        
        scheduleNextAdTimer()
        
        if let _alerts = Alert.getAlertsFromDefaults(){
            alerts = _alerts
        }
        
        if let idArray =  Alert.getAlertHistory(){
            alertHistory = idArray
        }
        timer = NSTimer.scheduledTimerWithTimeInterval(60.0, target: self, selector: "updateAlerts", userInfo: nil, repeats: true)
        timer?.fire()

        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, -60), forBarMetrics: .Default)
        setAppBadge()
        return true
    }

//    func enableCheckinButton(){
//        
//        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        
////        if (delegate.cmsConnection!.loggedIn) {
//            let user = "anfs"//delegate.cmsConnection?.username!
//            
//            var todayCheckins = NSUserDefaults.standardUserDefaults().integerForKey("TodayCheckins\(user)") ?? 0
//            let lastCheckinTime = NSUserDefaults.standardUserDefaults().objectForKey("LastCheckinTime\(user)" ) as? NSDate
//            
//            var interval = 3600.0 //default interval 1hr
//            if let lastDate = lastCheckinTime{
//                interval = NSDate().timeIntervalSinceDate(lastDate)
//                let calendar = NSCalendar.currentCalendar()
//                let comp = calendar.components(NSCalendarUnit.DayCalendarUnit, fromDate: lastCheckinTime!, toDate: NSDate(), options: NSCalendarOptions())
//                if comp.day >= 1 {
//                    todayCheckins = 0
//                    NSUserDefaults.standardUserDefaults().setInteger(todayCheckins, forKey: "TodayCheckins\(user)")
//                }
//                
//            }
//            var enabled = true
//            if todayCheckins < 3 && interval >= 3600{
//                enabled = true
//                test()
//            }else{
//                enabled = false
//                
//            }
//            println("enabled : \(enabled)")
//      //  }
//        
//        //        if todayCheckins < 3
//    }
//
//    func test(){
//        let delegate = UIApplication.sharedApplication().delegate as! AppDelegate
//        let userName = "aasf"//delegate.cmsConnection?.username!
//        
//        var todayCheckins = NSUserDefaults.standardUserDefaults().integerForKey("TodayCheckins\(userName)") ?? 0
//        todayCheckins = todayCheckins + 1
//        let defaults = NSUserDefaults.standardUserDefaults()
//        defaults.setInteger(todayCheckins, forKey: "TodayCheckins\(userName)")
//        defaults.setObject(NSDate(), forKey: "LastCheckinTime\(userName)")
//        defaults.synchronize()
//    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
	
	func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        
        locationManager.stopUpdatingLocation()
        var defaults = NSUserDefaults.standardUserDefaults()
        var allowGeofenceAlerts = defaults.boolForKey("allowGeofenceAlerts")
        
        if (allowGeofenceAlerts && CLLocationManager.significantLocationChangeMonitoringAvailable()) {
            //locationManager.startMonitoringSignificantLocationChanges()
        }
		
		if (!isShowingAd && adTimer != nil) {
			adTimerTimeLeft = adTimer!.fireDate.timeIntervalSinceNow
			adTimer!.invalidate()
		}
    }
    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
		
		var defaults = NSUserDefaults.standardUserDefaults()
		var allowGeofenceAlerts = defaults.boolForKey("allowGeofenceAlerts")
		
		if (allowGeofenceAlerts && CLLocationManager.significantLocationChangeMonitoringAvailable()) {
			//locationManager.stopMonitoringSignificantLocationChanges()
		}
		
		locationManager.startUpdatingLocation()
		
		// If the ads haven't been updated in the last 5 minutes, updated them again
		if (adsLastUpdated.timeIntervalSinceNow < -3600.0) {
			self.updateAds()
		}
		
    }
	
    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        var defaults = NSUserDefaults.standardUserDefaults()
        var allowGeofenceAlerts = defaults.boolForKey("allowGeofenceAlerts")
        
        if (allowGeofenceAlerts && CLLocationManager.significantLocationChangeMonitoringAvailable()) {
            //locationManager.stopMonitoringSignificantLocationChanges()
        }
        
        locationManager.startUpdatingLocation()
		
		if (adTimer != nil) {
			adTimer!.invalidate()
		}
		
		
		if (adViewController != nil) {
			if (adViewController!.ad != nil && adViewController!.ad!.ad_type == "video") {
				adViewController!.playVideo()
			}
//			adViewController!.dismissViewControllerAnimated(false,
//				completion: {
//					self.scheduleNextAdTimer()
//				}
//			)
		}
		
		if (!isShowingAd && adTimer != nil) {
//			if (adTimerTimeLeft < 10.0) { //don't show ads immediately after opening the app
//				adTimerTimeLeft = 10.0
//			}
			adTimer = NSTimer.scheduledTimerWithTimeInterval(adTimerTimeLeft, target: self, selector: "adTimerDidFire", userInfo: nil, repeats: false)
		}

		var tabBarController = self.window!.rootViewController as! UITabBarController
//		if (tabBarController.selectedIndex == 2) {
//			self.tabBarController(tabBarController, didSelectViewController: tabBarController.selectedViewController!)
//		}
}
    
	
    func locations(query:String) -> NSArray {
        return [];
    }
    
    // MARK: - Push notifications
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        var tokenString = deviceToken.hexString()
        cmsConnection!.registerDevice(tokenString as String, sender: self)
    }
    //nri error 5
    @available(iOS 8.0, *) //added this line- nri
    func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        application.registerForRemoteNotifications()
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print(error)
    }
    
    
    // MARK: - Region monitoring
    //nri error 6
    //func locationManager(manager: CLLocationManager!, didUpdateLocations locations: [AnyObject]!)
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
		
		//nri eror var newLocation = locations.last as? CLLocation
        let newLocation = locations.last
		if (currentLocation == nil || overallMonitoredRegion == nil) {
			cmsConnection!.getNearbyLocationsToMonitor(["lat":"\(newLocation!.coordinate.latitude)", "lng":"\(newLocation!.coordinate.longitude)"])
		}
        self.currentLocation = newLocation
		
        if (currentRegion == nil) {
            self.currentRegion = self.nearestRegion()
        }
    }
    
    func updateMonitoredRegions() {
        for region in locationManager.monitoredRegions {
            locationManager.stopMonitoringForRegion(region as! CLRegion)
        }
		
		var locationRadius = 0.0
		var defaults = NSUserDefaults.standardUserDefaults()
		var allowGeofenceAlerts = defaults.boolForKey("allowGeofenceAlerts")
		
		if (allowGeofenceAlerts) {
			for location in locations {
				var region = CLCircularRegion(center: CLLocationCoordinate2DMake(location.latitude, location.longitude), radius: 200.0, identifier: location.name)
				locationManager.startMonitoringForRegion(region)
				if (currentLocation != nil) {
					var distance = currentLocation!.distanceFromLocation(CLLocation(latitude: location.latitude, longitude: location.longitude))
					if (distance > locationRadius) {
						locationRadius = distance
					}
				}
			}
			
			if (currentLocation != nil) {
				var region = CLCircularRegion(center: currentLocation!.coordinate, radius: locationRadius, identifier: "monitoringRadius")
                
                //nri error 7
                //if (region != nil) {
                //self.overallMonitoredRegion = region!
                //locationManager.startMonitoringForRegion(region!)
                //}
                
                if (region .isKindOfClass(CLCircularRegion)) {
                    self.overallMonitoredRegion = region
                    locationManager.startMonitoringForRegion(region)
                }
			}
		}
    }
	
    func locationManager(manager: CLLocationManager!, didFailWithError error: NSError!) {
		print(error)
	}
	
	func locationManager(manager: CLLocationManager!, monitoringDidFailForRegion region: CLRegion!, withError error: NSError!) {
		print(error)
	}
	
	func locationWithName(name: String) -> Location? {
		for location in locations {
			if location.name == name {
				return location
			}
		}
		return nil
	}
	
	func locationManager(manager: CLLocationManager!, didEnterRegion region: CLRegion!) {
		var defaults = NSUserDefaults.standardUserDefaults()
		var allowGeofenceAlerts = defaults.boolForKey("allowGeofenceAlerts")
		if (allowGeofenceAlerts) {
			if (region == overallMonitoredRegion) {
				//This shouldn't really happen
			}
			else {
				var app = UIApplication.sharedApplication()
				var appState = app.applicationState
				if appState != UIApplicationState.Active {
					//app.cancelAllLocalNotifications()
					
					var notification = UILocalNotification()
					notification.alertBody = "Entered range of '\(region.identifier)'"
					notification.alertAction = "OK"
					notification.soundName = UILocalNotificationDefaultSoundName
					var location = locationWithName(region.identifier)
					if (location != nil) {
						notification.userInfo = ["name" : region.identifier]
					}
					app.presentLocalNotificationNow(notification)
				}
			}
			//		UIAlertView(title: "Region", message: "Entered range of \(region.identifier)", delegate: self, cancelButtonTitle: "OK").show()
			
		}
	}
	
    func locationManager(manager: CLLocationManager!, didExitRegion region: CLRegion!) {
		if (overallMonitoredRegion != nil && region.identifier == overallMonitoredRegion!.identifier && currentLocation != nil) {
			cmsConnection!.getNearbyLocationsToMonitor(["lat":"\(currentLocation!.coordinate.latitude)", "lng":"\(currentLocation!.coordinate.longitude)"])
		}
		
        
    }
	
	func application(application: UIApplication, didReceiveLocalNotification notification: UILocalNotification) {
		var tabBarController = self.window!.rootViewController as! UITabBarController
		tabBarController.selectedIndex = 2
		
		if (notification.userInfo != nil) {
			var homeNavigationController = tabBarController.viewControllers![2] as! UINavigationController
			homeNavigationController.popToRootViewControllerAnimated(false)
			var discountsViewController = homeNavigationController.viewControllers[0] as! DiscountsViewController
			
			var locationName = notification.userInfo!["name"] as? String
			if locationName != nil {
				var location = locationWithName(locationName!)
				if (location != nil) {
					discountsViewController.selectedLocation = location!
					discountsViewController.performSegueWithIdentifier("showLocation", sender: nil)
				}
			}
		}
	}
	
	func setCMSError(error: NSError) {
        
    }
    
    func loginSucceeded() {
        
    }
    
    func loginFailed() {
        
    }
    
    // MARK: - Facebook
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject?) -> Bool {
        FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)

        return FBSession.activeSession().handleOpenURL(url)
    }
	
	func applicationWillTerminate(application: UIApplication) {
		FBSession.activeSession().close()
        Alert.saveAlertsToDefaults(self.alerts)

	}
	
    func nearestRegion() -> String {
        var nearestRegion = ""
        var nearestDistance = 20000000.0 // ~ π*r of earth
        for (name, coords) in regions {
            if (currentLocation!.distanceFromLocation(CLLocation(latitude: coords.latitude, longitude: coords.longitude)) < nearestDistance) {
                nearestDistance = currentLocation!.distanceFromLocation(CLLocation(latitude: coords.latitude, longitude: coords.longitude))
                nearestRegion = name
            }
        }
        return nearestRegion
    }
    
    func updateAlerts(){
        self.cmsConnection!.getAlerts { (responseData, error) -> () in
            
            if error == nil && responseData != nil{
                for alert in responseData as! [Alert]{
                    //nrierror  8
                    //if !contains(self.alertHistory, alert.id)
                    if !self.alertHistory.contains(alert.id)
                    {
                        self.alerts.append(alert)
                        self.alertHistory.append(alert.id)
                    }
                }
                Alert.setAlertHistory(self.alertHistory)
                Alert.saveAlertsToDefaults(self.alerts)
                self.setAppBadge()
                NSNotificationCenter.defaultCenter().postNotificationName("AlertUpdateNotification", object: nil)
            }
        }
    }
    
    func setAppBadge(){
      UIApplication.sharedApplication().applicationIconBadgeNumber = getUnreadAlertCount()
    }
    func getUnreadAlertCount()-> Int{
        
        let filteredArray = self.alerts.filter{
            $0.isRead == false
        }
        return filteredArray.count
        
//            let storyboard = UIStoryboard(name: "Main", bundle: NSBundle.mainBundle()) as UIStoryboard
//            let initial = storyboard.instantiateInitialViewController() as? UITabBarController
//            var btn = initial?.tabBar.items?.first as? UITabBarItem
//            let lastVC = initial?.viewControllers?.last as? UINavigationController
//            let settingsView = lastVC?.topViewController
//            settingsView?.navigationController?.tabBarItem.badgeValue = "4"
//            lastVC?.tabBarItem.badgeValue = "3"
//            UIApplication.sharedApplication().applicationIconBadgeNumber = filteredArray.count
        
    }
    // Mark: - Ads
    
    func updateAds() {
        cmsConnection!.getAds(self)
    }
    
    func setAllAds(_ads: [Ad]) {
		adsLastUpdated = NSDate()
		self.ads = _ads
		dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
			
			for newAd in _ads {
				newAd.updateImage()
//				var needsImage = true
//				for oldAd in self.ads! {
//					if (oldAd.id == newAd.id) {
//						if (newAd.updated_at > oldAd.updated_at) {
//							newAd.updateImage()
//						}
//						else {
//							newAd.image = oldAd.image
//						}
//						needsImage = false
//						break;
//					}
//				}
//				if (needsImage) {
//					newAd.updateImage()
//				}
			}
			dispatch_async(dispatch_get_main_queue()) {
				self.ads = _ads
			}
		}
        //saveAds()
    }
	
//    func saveAds() {
//        var adsArray = NSMutableArray()
//        //convert the array into archivable objects
//        for ad in ads! {
//            adsArray.addObject(NSKeyedArchiver.archivedDataWithRootObject(ad))
//        }
//        adsArray.writeToFile(adsFile, atomically: true)
//        
//        var adsDirectory = documentsPath.stringByAppendingPathComponent("ads")
//        if (!NSFileManager.defaultManager().fileExistsAtPath(adsDirectory)) {
//            NSFileManager.defaultManager().createDirectoryAtPath(adsDirectory, withIntermediateDirectories: false, attributes: nil, error: nil)
//        }
//        
//        for ad in ads! {
//            if (ad.image != nil) {
//                //				var path = adsDirectory.stringByAppendingPathComponent("\(ad.id).jpg")
//                //				var image = ad.image!
//                UIImagePNGRepresentation(ad.image!).writeToFile(ad.filePath(), atomically: true)
//            }
//        }
//    }
//    
//    func savedAds() -> [Ad] {
//        var adsArray = NSArray(contentsOfFile: adsFile)
//        var _ads: [Ad] = []
//        if (adsArray != nil) {
//            for adData in adsArray! {
//                _ads += [NSKeyedUnarchiver.unarchiveObjectWithData(adData as NSData) as Ad]
//            }
//        }
//        for ad in _ads {
//            if (ad.ad_type == "image") {
//                ad.image = UIImage(contentsOfFile: ad.filePath())
//            }
//            else { //video
//                //var videoPath = ad.filePath()
//                //var image = UIImage(contentsOfFile: imagePath)
//                //ad.image = image
//            }
//        }
//        return _ads
//    }
	
    func adTimerDidFire() {
		if (rootViewController?.presentedViewController != nil) {
			scheduleNextAdTimer()
			return
		}
		isShowingAd = true
        if (ads != nil && ads!.count > 0) {
            adViewController = AdViewController()
            NSBundle.mainBundle().loadNibNamed("AdViewController", owner: adViewController, options: nil)
			if (adIndex >= ads!.count) {
				adIndex = 0
			}
			var ad = ads![adIndex++]
			if (ad.ad_type == "image" && ad.image == nil) {
				scheduleNextAdTimer()
			}
			else {
				adViewController!.ad = ad
				if (rootViewController != nil) {
					rootViewController!.presentViewController(adViewController!, animated: true, completion: nil)
				}
			}
        }
		else {
			scheduleNextAdTimer()
		}
    }
	
    func scheduleNextAdTimer() {
		isShowingAd = false
		adTimer?.invalidate()
        adTimer = NSTimer.scheduledTimerWithTimeInterval(60.0, target: self, selector: "adTimerDidFire", userInfo: nil, repeats: false)
		adViewController = nil
    }
    
    func tabBarController(tabBarController: UITabBarController, didSelectViewController viewController: UIViewController) {
        var tabItem = tabBarController.selectedIndex
		//if (tabItem == 4) { //Options tab
		var viewController = tabBarController.viewControllers![tabItem] as! UIViewController
		if (viewController is UINavigationController) {
			var navigationViewController = viewController as! UINavigationController
			navigationViewController.popToRootViewControllerAnimated(false)
		}
		//}
		
    }
}
