//
//  CategoriesPopoverViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/27/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class CategoriesPopoverViewController: CategoriesViewController {

//	var appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as AppDelegate
//	var categories: [EstablishmentCategory] = []
//	@IBOutlet weak var loadingIndicator: UIActivityIndicatorView!

	var locationsTableViewController: LocationsTableViewController?
	
	override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
	
//	override func viewWillAppear(animated: Bool) {
//		super.viewWillAppear(animated)
//	}
//	
//	override func viewDidAppear(animated: Bool) {
//		super.viewDidAppear(animated)
//	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return categories.count + 1
    }

	override func tableView(tableView: UITableView?, cellForRowAtIndexPath indexPath: NSIndexPath?) -> UITableViewCell {
		let cell = tableView!.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath!) as! UITableViewCell
		
		var category: EstablishmentCategory?
		if (indexPath!.row == 0) {
			category = nil
		}
		else {
			category = categories[indexPath!.row - 1]
		}
		var title = NSMutableAttributedString(string: (category == nil ? "All".localized : category!.name), attributes: [NSFontAttributeName:UIFont.systemFontOfSize(17.0), NSForegroundColorAttributeName:UIColor.whiteColor()])
        if category != nil{
		var locations = NSMutableAttributedString(string: " (\(category!.location_count))", attributes: [NSFontAttributeName:UIFont.systemFontOfSize(17.0), NSForegroundColorAttributeName:UIColor.whiteColor()])
		locations.setAttributes([NSFontAttributeName:UIFont.boldSystemFontOfSize(17.0), NSForegroundColorAttributeName:UIColor.whiteColor()], range: NSMakeRange(2, locations.length-3))
		title.appendAttributedString(locations)
        }
		cell.textLabel!.attributedText = title
		
		return cell
	}
	
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		var category: EstablishmentCategory?
		if (indexPath.row == 0) {
			category = nil
			locationsTableViewController!.category = "All"
		}
		else {
			category = categories[indexPath.row - 1]
			locationsTableViewController!.category = category!.name
		}

		self.locationsTableViewController!.updateLocations()
		self.dismissViewControllerAnimated(true, completion: nil)
	}

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */
	
}
