//
//  CategoriesViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/13/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class CategoriesViewController: UITableViewController {
	
	var appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
	var categories: [EstablishmentCategory] = []
	@IBOutlet weak var regionButton: UIButton!
	@IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
	@IBOutlet weak var titleLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
		
		self.refreshControl = UIRefreshControl()
		self.refreshControl!.addTarget(self, action: Selector("updateCategories"), forControlEvents: UIControlEvents.ValueChanged)
		applyStyles(true)
		
		//self.title = "Categories"
		if (self.titleLabel != nil) {
			self.titleLabel.text = "Categories".localized
		}
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		
		if (regionButton != nil) {
			regionButton.setTitle(appDelegate.currentRegion, forState: UIControlState.Normal)
		}
		
	}
	
	override func viewDidAppear(animated: Bool) {
		super.viewDidAppear(animated)
		
		if categories.count == 0 {
			updateCategories()
		}		

	}
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }

	override func tableView(tableView: UITableView?, cellForRowAtIndexPath indexPath: NSIndexPath?) -> UITableViewCell {
        let cell = tableView!.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath!) as! UITableViewCell
		
		var category = categories[indexPath!.row]
		var title = NSMutableAttributedString(string: category.name, attributes: [NSFontAttributeName:UIFont.systemFontOfSize(17.0), NSForegroundColorAttributeName:UIColor.whiteColor()])
		var locations = NSMutableAttributedString(string: " (\(category.location_count))", attributes: [NSFontAttributeName:UIFont.systemFontOfSize(17.0), NSForegroundColorAttributeName:UIColor.whiteColor()])
		locations.setAttributes([NSFontAttributeName:UIFont.boldSystemFontOfSize(17.0), NSForegroundColorAttributeName:UIColor.whiteColor()], range: NSMakeRange(2, locations.length-3))
		title.appendAttributedString(locations)
		cell.textLabel!.attributedText = title

        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView!, moveRowAtIndexPath fromIndexPath: NSIndexPath!, toIndexPath: NSIndexPath!) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
		if (segue.destinationViewController is LocationsTableViewController) {
			var locationsViewController = segue.destinationViewController as! LocationsTableViewController
			//nri error
            //var selectedPath = self.tableView.indexPathForSelectedRow()!
            var selectedPath = self.tableView.indexPathForSelectedRow!
			locationsViewController.context = "category"
			locationsViewController.category = self.categories[selectedPath.row].name
		}
		else if (segue.destinationViewController is RegionPickerViewController) {
			var destinationViewController = segue.destinationViewController as! RegionPickerViewController
			destinationViewController.categoriesViewController = self
		}
    }

	func updateCategories() {
		loadingIndicator.startAnimating()
		appDelegate.cmsConnection!.getCategories((appDelegate.currentRegion == nil) ? "" : appDelegate.currentRegion!, sender: self)
	}
	
	func setAllCategories(_categories: Array<EstablishmentCategory>) {
		self.categories = _categories
		self.tableView.reloadData()
		self.refreshControl?.endRefreshing()
		loadingIndicator.stopAnimating()
	}
	
	

}
