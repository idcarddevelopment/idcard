//
//  FacebookAdditions.m
//  iDcard
//
//  Created by Philip Kirkham on 12/4/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

#import "FacebookAdditions.h"


@implementation FacebookAdditions

//+ (NSMutableDictionary<FBGraphObject> *)fbGraphObjectWithType:fbGraphObjectWithType:(NSString *)aType title:(NSString *)aTitle image:(NSString *)anImage url:(NSString *)aUrl description:(NSString *)aDescription {
//	return nil;
//}

+ (void)checkIn:(NSString *)aType
          title:(NSString *)aTitle
          image:(NSString *)anImage
            url:(NSString *)aUrl
    description:(NSString *)aDescription
checkInViewController:(UIViewController<FacebookCheckInController> *)aCheckInViewController {
    
#pragma mark - Using custom UI

//    NSMutableDictionary<FBOpenGraphObject> *object = [FBGraphObject openGraphObjectForPostWithType:aType
//                                                                                             title:aTitle
//                                                                                             image:anImage
//                                                                                               url:aUrl
//                                                                                       description:@""];
    
    //	NSMutableDictionary<FBGraphObject> *object = [@{@"sponsor" : aUrl} mutableCopy];
    
    // Post custom object
//    [FBRequestConnection startForPostOpenGraphObject:object completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//        if(!error) {
//            // get the object ID for the Open Graph object that is now stored in the Object API
//            NSString *objectId = [result objectForKey:@"id"];
//            
//            id<FBOpenGraphAction> action = (id<FBOpenGraphAction>)[FBGraphObject graphObject];
//            [action setObject:objectId forKey:@"sponsor"];
//            [action setObject:@"true" forKey: @"fb:explicitly_shared"];
//            [action setObject:aDescription forKey: @"message"];
//            
//            
//            [FBRequestConnection startForPostWithGraphPath:@"/me/theidcard:check_in_at" graphObject:action completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//                if(!error) {
//                    //					[[[UIAlertView alloc] initWithTitle:@"OG story posted"
//                    //												message:@"Check your Facebook profile or activity log to see the story."
//                    //											   delegate:self
//                    //									  cancelButtonTitle:@"OK!"
//                    //									  otherButtonTitles:nil] show];
//                    [aCheckInViewController didPublishFacebookStatus:YES];
//                } else {
//                    [aCheckInViewController didPublishFacebookStatus:NO];
//                }
//            }];
//            
//            
//            
//        } else {
//            [aCheckInViewController didPublishFacebookStatus:NO];
//        }
//    }];
    
    
#pragma mark - Using facebook share dialog
    // Create an object
    NSDictionary *properties = @{
                                 @"og:type": aType,
                                 @"og:title": aTitle,
                                 @"og:description": aDescription,
                                 @"og:image":anImage,
                                 @"og:url":aUrl
                                 };
    FBSDKShareOpenGraphObject *obj = [FBSDKShareOpenGraphObject objectWithProperties:properties];
    // Create an action
    FBSDKShareOpenGraphAction *action = [[FBSDKShareOpenGraphAction alloc] init];
    action.actionType = @"islediscountcard:check_in_at";
    [action setObject:obj forKey:@"sponsor"];
    // Create the content
    FBSDKShareOpenGraphContent *content = [[FBSDKShareOpenGraphContent alloc] init];
    content.action = action;
    content.previewPropertyName = @"islediscountcard:sponsor";
    
    id<UIApplicationDelegate> delegate  = [[UIApplication sharedApplication]delegate];
    [FBSDKShareDialog showFromViewController:aCheckInViewController
                                 withContent:content
                                    delegate: aCheckInViewController];

    
    //	FBRequestConnection *requestConnection = [FBRequestConnection startForPostWithGraphPath:@"me/islediscountcard:check_in_at"
    //																				graphObject:object
    //																		  completionHandler:^(FBRequestConnection *connection,
    //																							  id result,
    //																							  NSError *error) {
    //																			  if (error) {
    //																				  [[[UIAlertView alloc] initWithTitle:@"Facebook Error" message:@"Sorry, we couldn't communicate with Facebook.  Please try again." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    //																				  //self.waitingView.hidden = true
    //																			  }
    //																			  else {
    //																				  [requestConnection
    //																				  //var resultObj = result as FBGraphObject
    //																				  //self.waitingView.hidden = false
    //																				  //var cmsConnection = self.appDelegate.cmsConnection
    //																				  //cmsConnection!.checkin(self.location!, type: "facebook", sender: self)
    //																			  }
    //																		  }];
    //	NSLog(@"%@", requestConnection);
}


+ (void)photoCheckIn:(NSString *)aType
               title:(NSString *)aTitle
               image:(NSString *)anImage
                 url:(NSString *)aUrl
         description:(NSString *)aDescription
               photo:(UIImage *)aPhoto
checkInViewController:(UIViewController<FacebookCheckInController> *)aCheckInViewController {
    
    // Construct an FBSDKSharePhoto
    FBSDKSharePhoto *photo = [[FBSDKSharePhoto alloc] init];
    photo.image = aPhoto;
    photo.userGenerated = YES;
    
    // Create an object
    NSMutableDictionary *properties = [@{
                                 @"og:type": aType,
                                 @"og:title": aTitle,
                                 @"og:description": aDescription,
                                 @"og:url":aUrl,
                                 } mutableCopy];
    
    FBSDKShareOpenGraphObject *object = [FBSDKShareOpenGraphObject objectWithProperties:properties];
    
    FBSDKShareOpenGraphAction *action = [[FBSDKShareOpenGraphAction alloc] init];
    action.actionType = @"islediscountcard:check_in_at";
    [action setObject:object forKey:@"sponsor"];
    
    // Add the photo to the action. Actions
    // can take an array of images.
    [action setArray:@[photo] forKey:@"image"];
    
    // Create the content
    FBSDKShareOpenGraphContent *content = [[FBSDKShareOpenGraphContent alloc] init];
    content.action = action;
    content.previewPropertyName = @"islediscountcard:sponsor";
    
    [FBSDKShareDialog showFromViewController:aCheckInViewController
                                 withContent:content
                                    delegate:aCheckInViewController];
    
//        [FBRequestConnection startForUploadStagingResourceWithImage:aPhoto
//            completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//                    if(!error) {
//                        properties[@"og:image"] = [result objectForKey:@"uri"];
//
//                                      }
//                    else{
//                        [aCheckInViewController didPublishFacebookStatus: NO];
//                    }
//                                  }];
    
    // Create an action

    
}


//+ (void)photoCheckIn:(NSString *)aType
//               title:(NSString *)aTitle
//               image:(NSString *)anImage
//                 url:(NSString *)aUrl
//         description:(NSString *)aDescription
//               photo:(UIImage *)aPhoto
//checkInViewController:(UIViewController<FacebookCheckInController> *)aCheckInViewController {
//    
//    NSMutableDictionary<FBOpenGraphObject> *object = [FBGraphObject openGraphObjectForPostWithType:aType
//                                                                                             title:aTitle
//                                                                                             image:anImage
//                                                                                               url:aUrl
//                                                                                       description:@""];
//    
//    [FBRequestConnection startForUploadStagingResourceWithImage:aPhoto
//                                              completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//                                                  if(!error) {
//                                                      object[@"image"] = @[@{@"url": [result objectForKey:@"uri"], @"user_generated" : @"true" }];
//                                                      
//                                                      
//                                                      [FBRequestConnection startForPostOpenGraphObject:object completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//                                                          if(!error) {
//                                                              NSString *objectId = [result objectForKey:@"id"];
//                                                              
//                                                              id<FBOpenGraphAction> action = (id<FBOpenGraphAction>)[FBGraphObject graphObject];
//                                                              [action setObject:objectId forKey:@"sponsor"];
//                                                              [action setObject:@"true" forKey: @"fb:explicitly_shared"];
//                                                              [action setObject:aDescription forKey: @"message"];
//                                                              
//                                                              [FBRequestConnection startForPostWithGraphPath:@"/me/theidcard:check_in_at" graphObject:action completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//                                                                  if(!error) {
//                                                                      [aCheckInViewController didPublishFacebookStatus: (error == nil)? YES: NO];
//                                                                      
//                                                                      //add the status update as well
//                                                                      //																	  if ([[aDescription stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
//                                                                      //																		  [aCheckInViewController didPublishFacebookStatus:YES];
//                                                                      //																	  }
//                                                                      //																	  else {
//                                                                      //																		  [FBRequestConnection startForPostStatusUpdate:aDescription
//                                                                      //																									  completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//                                                                      //																										  if (!error) {
//                                                                      //																											  [aCheckInViewController didPublishFacebookStatus:YES];
//                                                                      //																										  } else {
//                                                                      //																											  [aCheckInViewController didPublishFacebookStatus:NO];
//                                                                      //																										  }
//                                                                      //																									  }];
//                                                                      //																	  }
//                                                                      
//                                                                      //																	  NSMutableDictionary<FBOpenGraphObject> *statusObject = [FBGraphObject openGraphObjectForPostWithType:aType
//                                                                      //																																									 title:aTitle
//                                                                      //																																									 image:nil
//                                                                      //																																									   url:aUrl
//                                                                      //																																							   description:aDescription];
//                                                                      //																	  [FBRequestConnection startForPostOpenGraphObject:statusObject completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//                                                                      //																		  if(!error) {
//                                                                      //																			  NSString *objectId = [result objectForKey:@"id"];
//                                                                      //																			  
//                                                                      //																			  id<FBOpenGraphAction> action = (id<FBOpenGraphAction>)[FBGraphObject graphObject];
//                                                                      //																			  [action setObject:objectId forKey:@"sponsor"];
//                                                                      //																			  [action setObject:@"true" forKey: @"fb:explicitly_shared"];
//                                                                      //																			  
//                                                                      //																			  [FBRequestConnection startForPostWithGraphPath:@"/me/islediscountcard:check_in_at" graphObject:action completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
//                                                                      //																				  if(!error) {
//                                                                      //																					  [aCheckInViewController didPublishFacebookStatus:YES];
//                                                                      //																				  } else {
//                                                                      //																					  [aCheckInViewController didPublishFacebookStatus:NO];
//                                                                      //																				  }
//                                                                      //																			  }];
//                                                                      //																			  
//                                                                      //																			  
//                                                                      //																			  
//                                                                      //																		  } else {
//                                                                      //																			  [aCheckInViewController didPublishFacebookStatus:NO];
//                                                                      //																		  }
//                                                                      //																	  }];
//                                                                      
//                                                                  } else {
//                                                                      [aCheckInViewController didPublishFacebookStatus:NO];
//                                                                  }
//                                                              }];
//                                                              
//                                                              
//                                                              
//                                                          } else {
//                                                              [aCheckInViewController didPublishFacebookStatus:NO];
//                                                          }
//                                                      }];
//                                                      
//                                                  } else {
//                                                      [aCheckInViewController didPublishFacebookStatus:NO];
//                                                  }
//                                              }];
//}

@end

