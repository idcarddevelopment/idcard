//
//  OptionsViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/14/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class OptionsViewController: UITableViewController, UIAlertViewDelegate {
	
    @IBOutlet weak var alertsLabel: UILabel!
	var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
	var signOutAlert: UIAlertView?
	@IBOutlet weak var emailLabel: UILabel!
	@IBOutlet weak var signInLabel: UILabel!
    @IBOutlet weak var contentView: UIView!
    var badge:CustomBadge?
    
	override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
		alertsLabel.text = "Alerts".localized
		signOutAlert = UIAlertView(title: "Sign Out".localized, message: "Are you sure you want to sign out?".localized, delegate: self, cancelButtonTitle: "Cancel".localized, otherButtonTitles: "OK".localized)
		applyStyles(true)
         NSNotificationCenter.defaultCenter().addObserver(self, selector: "setBadge", name: "AlertUpdateNotification", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "setBadge", name: "AlertReadNotification", object: nil)
    }
    
    func setBadge(){
        let count:String?
        
        if appDelegate.getUnreadAlertCount() != 0{
            count = "\(appDelegate.getUnreadAlertCount())"
        }else{
            count = nil
        }
        if count != nil{
            
            badge?.hidden = false
            badge?.badgeText = count
            badge?.setNeedsDisplay()
            badge?.autoBadgeSizeWithString(count)
        }else{
            badge?.hidden = true
        }
    }
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		
		var defaults = NSUserDefaults.standardUserDefaults()
		var username = defaults.valueForKey("username") as! String?
		var cmsConnection = appDelegate.cmsConnection
		print("Username: \(username!)")
		
		if (cmsConnection!.loggedIn) {
			signInLabel.text = "Sign Out".localized
			emailLabel.text = username!
		}
		else {
			signInLabel.text = "Sign In".localized
			emailLabel.text = ""
			print(emailLabel.text)
		}
		//self.title = "Menu"
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath){
        if indexPath.row == 0{
            badge = CustomBadge(string: "10")
            badge?.hidden = true
            badge!.frame = CGRectMake(80, 7, 30, 30)
            cell.addSubview(badge!)
            setBadge()
        }
    }
	
	override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
		if (indexPath.row == 7) { //Shop
			UIApplication.sharedApplication().openURL(NSURL(string: "http://theidcard.com/?post_type=product")!)
		}
		else if (indexPath.row == 8) { //Sign in or out
			var cmsConnection = appDelegate.cmsConnection
			if (cmsConnection!.loggedIn) {
				signOutAlert!.show()
			}
			else {
                self.tabBarController?.selectedIndex = 2
				self.performSegueWithIdentifier("login", sender: self)
			}
		}
		tableView.deselectRowAtIndexPath(indexPath, animated: false)
	}
	
		
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
		if (segue.destinationViewController is HTMLViewController) {
			var viewController = segue.destinationViewController as! HTMLViewController
			if (segue.identifier == "about") {
				viewController.fileName = "about-us"
				viewController.title = "About Us".localized
			}
		}
		else if (segue.destinationViewController is AdvertisingViewController) {
			var viewController = segue.destinationViewController as! AdvertisingViewController
			if (segue.identifier == "advertising") {
				viewController.title = "Free Advertising"
			}
			else if (segue.identifier == "fundraiser") {
				viewController.title = "Fundraiser Info"
			}
		}
        else if (segue.destinationViewController is LoginViewController) {
            var viewController = segue.destinationViewController as! LoginViewController
            viewController.optionsViewController = self
        }
    }
	
	func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
		print("Clicked button at \(buttonIndex)")
		if (buttonIndex == 0) { // Cancel
			
		}
		else if (buttonIndex == 1) { // OK

            
			var cmsConnection = appDelegate.cmsConnection
			cmsConnection!.logout(self)
			signInLabel.text = "Sign In"
			emailLabel.text = ""
		}
	}
	
	
}
