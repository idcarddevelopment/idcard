//
//  ContactViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/20/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController {
	
	var appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()
		
		applyStyles(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
	@IBAction func phoneCall(sender: AnyObject) {
		UIApplication.sharedApplication().openURL(NSURL(string: "tel:\(appDelegate.contactPhone)")!)
	}

	@IBAction func sendEmail(sender: AnyObject) {
		UIApplication.sharedApplication().openURL(NSURL(string: "mailto:\(appDelegate.contactEmail)")!)
	}
	
	@IBAction func openMap(sender: AnyObject) {
		appDelegate.contactMapItem.openInMapsWithLaunchOptions(nil)
	}
	
	@IBAction func openFacebook(sender: AnyObject) {
		var appURL = NSURL(string: "fb://profile/\(appDelegate.contactFacebookId)")
		if (UIApplication.sharedApplication().canOpenURL(appURL!)) {
			UIApplication.sharedApplication().openURL(appURL!)
		}
		else {
			UIApplication.sharedApplication().openURL(NSURL(string: "https://www.facebook.com/\(appDelegate.contactFacebookId)")!)
		}
	}
	
	@IBAction func openTwitter(sender: AnyObject) {
		var appURL = NSURL(string: "twitter:///user?screen_name=\(appDelegate.contactTwitterId)")
		if (UIApplication.sharedApplication().canOpenURL(appURL!)) {
			UIApplication.sharedApplication().openURL(appURL!)
		}
		else {
			UIApplication.sharedApplication().openURL(NSURL(string: "https://twitter.com/\(appDelegate.contactTwitterId)")!)
		}
	}
	
	@IBAction func openYoutube(sender: AnyObject) {
		var appURL = NSURL(string: "youtube://www.youtube.com/channel/\(appDelegate.contactYoutubeId)")
		if (UIApplication.sharedApplication().canOpenURL(appURL!)) {
			UIApplication.sharedApplication().openURL(appURL!)
		}
		else {
			UIApplication.sharedApplication().openURL(NSURL(string: "https://www.youtube.com/channel/\(appDelegate.contactYoutubeId)")!)
		}
	}
	
	@IBAction func openInstagram(sender: AnyObject) {
		var appURL = NSURL(string: "instagram://user?username=\(appDelegate.contactInstagramId)")
		if (UIApplication.sharedApplication().canOpenURL(appURL!)) {
			UIApplication.sharedApplication().openURL(appURL!)
		}
		else {
			UIApplication.sharedApplication().openURL(NSURL(string: "http://instagram.com/\(appDelegate.contactInstagramId)")!)
		}
	}
	
	
	
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

	//NSURL(string: "tel:\(phone)")
	
	//UIApplication.sharedApplication().openURL(location.phoneURL()!)
}
