//
//  HTMLViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/14/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit
import WebKit

class HTMLViewController: UIViewController {
	
	@IBOutlet weak var webView: UIWebView!
	
	var fileName: String = "" {
		didSet {
			reload()
		}
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		webView.opaque = false
		webView.backgroundColor = UIColor.clearColor()
		applyStyles(true)
		reload()
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	func reload() {
//		var filePath = NSBundle.mainBundle().pathForResource(fileName, ofType: "html")
        let availableLocalizations = NSBundle.mainBundle().localizations
        /*ajith error
let userPrefered = NSBundle.preferredLocalizationsFromArray(availableLocalizations!, forPreferences: NSLocale.preferredLanguages()) 
        
        var filePath = NSBundle.mainBundle().pathForResource(fileName, ofType: "html", inDirectory: userPrefered[0].stringByAppendingPathExtension("lproj"), forLocalization: userPrefered[0] as? String)
        
        if (NSFileManager.defaultManager().fileExistsAtPath(filePath!)) {
        var htmlContent = NSString(contentsOfFile: filePath!, encoding: NSUTF8StringEncoding, error: nil)
        if (self.webView != nil) {
        self.webView.loadHTMLString(htmlContent as! String, baseURL: NSURL.fileURLWithPath(NSBundle.mainBundle().bundlePath))
        }
        }
        */
        
        //ajith error
        let userPrefered = NSBundle.preferredLocalizationsFromArray(availableLocalizations, forPreferences: NSLocale.preferredLanguages())
        
        var filePath = NSBundle.mainBundle().URLForResource(fileName,  withExtension: "html", subdirectory: userPrefered[0]+".lproj", localization: userPrefered[0] as? String)
            
           // pathForResource(fileName, ofType: "html", inDirectory: userPrefered[0].stringByAppendingPathExtension("lproj"), forLocalization: userPrefered[0] as? String)

		if (NSFileManager.defaultManager().fileExistsAtPath((filePath?.path!)!)) {
            var htmlContent = ""
            do {
                 htmlContent = try String(contentsOfFile: (filePath?.path)!, encoding: NSUTF8StringEncoding)
            } catch let error as NSError {
                print(error.localizedDescription)
            }
            //var htmlContent = String(contentsOfFile: filePath?.path, usedEncoding: NSUTF8StringEncoding)//: filePath, encoding: NSUTF8StringEncoding, error: nil)
			//var htmlContent = NSString(contentsOfURL: filePath!, encoding: NSUTF8StringEncoding, error: nil)
			if (self.webView != nil) {
				self.webView.loadHTMLString(htmlContent , baseURL: NSURL.fileURLWithPath(NSBundle.mainBundle().bundlePath))
			}
		}
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
