//
//  Alert.swift
//  iDcard
//
//  Copyright (c) 2015 DMG❖Bluegill. All rights reserved.
//

import UIKit

class Alert: NSObject {
   
    var id = 0
    var title = ""
    var message = ""
    var published = 0
    var updated_at = "0000-01-01T00:00:00.000Z"
    var created_at = "0000-01-01T00:00:00.000Z"
    var isRead = false
    
    override init() {
        
    }
    required init(coder aDecoder: NSCoder) {
        self.id  = aDecoder.decodeObjectForKey("id") as! Int
        self.title  = aDecoder.decodeObjectForKey("title") as! String
        self.message  = aDecoder.decodeObjectForKey("message") as! String
        self.updated_at  = aDecoder.decodeObjectForKey("updated_at") as! String
        self.created_at  = aDecoder.decodeObjectForKey("created_at") as! String
        self.isRead  = aDecoder.decodeBoolForKey("isRead")
    }
    
    func encodeWithCoder(aCoder: NSCoder!) {
        aCoder.encodeObject(id, forKey:"id")
        aCoder.encodeObject(title, forKey:"title")
        aCoder.encodeObject(message, forKey:"message")
        aCoder.encodeObject(updated_at, forKey:"updated_at")
        aCoder.encodeObject(created_at, forKey:"created_at")
        aCoder.encodeBool(isRead, forKey: "isRead")
    }
    
    class func saveAlertsToDefaults(alerts:[Alert]?){
        
        if alerts != nil{
        let alertData = NSKeyedArchiver.archivedDataWithRootObject(alerts!)
        NSUserDefaults.standardUserDefaults().setObject(alertData, forKey: "iDcard.AlertData")
        NSUserDefaults.standardUserDefaults().synchronize()
        }
    }
    
    class func getAlertsFromDefaults()->[Alert]?{
        let alertData = NSUserDefaults.standardUserDefaults().objectForKey("iDcard.AlertData") as? NSData
        
        if let data = alertData{
            var alerts = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? [Alert]
            return alerts
        }
        return nil
    }
    
    class func getAlertHistory()->[Int]?{
        return NSUserDefaults.standardUserDefaults().objectForKey("iDcard.AlertHistory") as? [Int]
    }
    class func setAlertHistory(idArray:[Int]){
        NSUserDefaults.standardUserDefaults().setObject(idArray, forKey: "iDcard.AlertHistory")
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    override func isEqual(object: AnyObject?) -> Bool {
        if let alert = object as? Alert{
        return id == alert.id
        }
        return false
    }
}
