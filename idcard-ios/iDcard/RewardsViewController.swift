//
//  RewardsViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/6/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class RewardsViewController: UITableViewController,UICollectionViewDataSource {

	var appDelegate: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
	var rewards: Array<Reward> = []
	var availablePoints:Int = 0
	var leaders = [] as NSArray

	@IBOutlet weak var availablePointsLabel: UILabel!
	@IBOutlet weak var leader1PointsLabel: UILabel!
	@IBOutlet weak var leader2PointsLabel: UILabel!
	@IBOutlet weak var leader3PointsLabel: UILabel!
	@IBOutlet weak var leader1NameLabel: UILabel!
	@IBOutlet weak var leader2NameLabel: UILabel!
	@IBOutlet weak var leader3NameLabel: UILabel!
	
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
		
		self.refreshControl = UIRefreshControl()
		self.refreshControl!.addTarget(self, action: Selector("updateRewards"), forControlEvents: UIControlEvents.ValueChanged)
		applyStyles(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	override func viewWillAppear(animated: Bool) {
		super.viewWillAppear(animated)
		
		if (rewards.count == 0 || appDelegate.rewardsHaveChanged) {
            //NSThread.detachNewThreadSelector("updateRewards", toTarget: self, withObject: nil)
			updateRewards()
		}

	}

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView?) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView?, numberOfRowsInSection section: Int) -> Int {
        return rewards.count
    }

    override func tableView(tableView: UITableView?, cellForRowAtIndexPath indexPath: NSIndexPath?) -> UITableViewCell {
        let cell = tableView!.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath!) as! UITableViewCell
		
		var imageView = cell.viewWithTag(100) as! UIImageView
		var titleLabel = cell.viewWithTag(101) as! UILabel
		var pointsLabel = cell.viewWithTag(102) as! UILabel
        var inventoryLabel = cell.viewWithTag(103) as! UILabel
		
		var reward = rewards[indexPath!.row]
		
		if (reward.photo != nil) {
			imageView.image = reward.photo!
		}
        else {
            imageView.image = nil
        }
		titleLabel.text = reward.name
        let pointsText = "Points".localized
		pointsLabel.text = "\(reward.points) \(pointsText)"
        let inventoryText = "Inventory".localized
		inventoryLabel.text = "\(reward.inventory) \(inventoryText)"

		if (reward.photo != nil) {
			imageView.image = reward.photo!
		}
		else {
			imageView.image = nil
			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
				var newReward = reward
                let data = NSData(contentsOfURL: NSURL(string: reward.photo_url)!)
                if let _data = data{
                   	var newPhoto = UIImage(data: _data)
                    dispatch_async(dispatch_get_main_queue()) {
                        newReward.photo = newPhoto
                        var visibleCell = tableView!.cellForRowAtIndexPath(indexPath!)
                        if (visibleCell != nil) {
                            imageView.image = newPhoto
                        }
                    }
                }
			
			}
		}
		imageView.layer.minificationFilter = kCAFilterTrilinear

        // Configure the cell...
		var selectedView = UIView()
		selectedView.backgroundColor = UIColor.highlightColor()
		cell.selectedBackgroundView = selectedView
		
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView!, canEditRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView!, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath!) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView!, moveRowAtIndexPath fromIndexPath: NSIndexPath!, toIndexPath: NSIndexPath!) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView!, canMoveRowAtIndexPath indexPath: NSIndexPath!) -> Bool {
        // Return NO if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation
 
    // In a storyboard-based application, you will often want to do a little preparation before navigation
	override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
		var detailViewController = segue.destinationViewController as! RewardsDetailViewController
        //nri error
		//var selectedPath = self.tableView.indexPathForSelectedRow()!
        var selectedPath = self.tableView.indexPathForSelectedRow!
		detailViewController.reward = self.rewards[selectedPath.row]
		detailViewController.availablePoints = availablePoints
	}

	// MARK: - CMSConnectionDelegate
	
	func setCMSError(error: NSError) {
		UIAlertView(title: "Error".localized, message: error.localizedDescription, delegate: self, cancelButtonTitle: "OK").show()
	}
	
	func updateRewards() {
        loadingIndicator.startAnimating()
		appDelegate.cmsConnection!.getRewards(self)
		appDelegate.rewardsHaveChanged = false
	}
	
	func setAllRewards(_rewards: Array<Reward>) {
        loadingIndicator.stopAnimating()
		self.refreshControl!.endRefreshing()
		self.rewards = _rewards
		self.tableView.reloadData()
	}
	
	func setAllAvailablePoints(_points: Int) {
		self.availablePoints = _points
		availablePointsLabel.text = "\(_points)"
	}
	
	func setAllLeaders(_leaders: NSArray) {
		self.leaders = _leaders
        self.collectionView.reloadData()
		
//		if (leaders.count >= 1) {
//			var leader = leaders[0] as! NSDictionary
//			leader1PointsLabel.text = String(leader["available_points"] as! Int!)
//			leader1NameLabel.text = String(leader["nickname"] as! String!)
//		}
//		else {
//			leader1PointsLabel.text = ""
//			leader1NameLabel.text = ""
//		}
//		if (leaders.count >= 2) {
//			var leader = leaders[1] as! NSDictionary
//			leader2PointsLabel.text = String(leader["available_points"] as! Int!)
//			leader2NameLabel.text = String(leader["nickname"] as! String!)
//		}
//		else {
//			leader2PointsLabel.text = ""
//			leader2NameLabel.text = ""
//		}
//		if (leaders.count >= 3) {
//			var leader = leaders[2] as! NSDictionary
//			leader3PointsLabel.text = String(leader["available_points"] as! Int!)
//			leader3NameLabel.text = String(leader["nickname"] as! String!)
//		}
//		else {
//			leader3PointsLabel.text = ""
//			leader3NameLabel.text = ""
//		}
	}

	@IBAction func showRewardInfo(sender: AnyObject) {
		UIAlertView(title: "About Rewards".localized, message: "Rack up points and redeem them for rewards. Limit one check-in per hour, three check-ins per day.".localized, delegate: nil, cancelButtonTitle: "OK").show()
	}
	
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
       return self.leaders.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell{
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("LeaderCell", forIndexPath: indexPath) as! UICollectionViewCell
        var backgroundImage = UIImage(named: "background-texture")
        var backgroundColor = UIColor(patternImage: backgroundImage!)
        let view = cell.viewWithTag(100)
//        view!.backgroundColor = backgroundColor
    
        let pointLabel = cell.viewWithTag(1) as! UILabel
        let nameLabel = cell.viewWithTag(2) as! UILabel
        let leader = self.leaders[indexPath.row] as! NSDictionary
        pointLabel.text = String(leader["available_points"] as! Int!)
        nameLabel.text = String(leader["nickname"] as! String!)
        return cell
    }

}
