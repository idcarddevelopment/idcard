//
//  OrientationViewController.swift
//  iDcard
//
//  Created by Philip Kirkham on 10/1/14.
//  Copyright (c) 2014 DMG❖Bluegill. All rights reserved.
//

import UIKit

class OrientationViewController: UIViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
	
	var dataObject: AnyObject?
	var pageController:UIPageViewController?
	
	var viewControllers:NSArray = NSBundle.mainBundle().loadNibNamed("Orientation", owner: nil, options: nil)
	var currentIndex = 0
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		for viewController in viewControllers as! [OrientationPanelViewController] {
			viewController.orientationViewController = self
		}
		
		pageController = UIPageViewController(transitionStyle: UIPageViewControllerTransitionStyle.Scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.Horizontal, options: nil)
		pageController!.dataSource = self
		
		pageController!.view.frame = self.view.bounds
		
//		var viewControllerObject = viewControllerAtIndex(0)!
//		var viewControllers = [viewControllerObject]
        
        
        
        //erroe nri
        //pageController!.setViewControllers([viewControllers[0]], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
        
		pageController!.setViewControllers(viewControllers[0] as? [UIViewController] , direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
		
		addChildViewController(pageController!)
		view.addSubview(pageController!.view)
		pageController?.didMoveToParentViewController(self)
		
		
		
//		ViewController *viewControllerObject = [self viewControllerAtIndex:0];
//		
//		NSArray *viewControllers = [NSArray arrayWithObject:viewControllerObject];
//		
//		[self.pageControllersetViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForwardanimated:NOcompletion:nil];
//		
//		[selfaddChildViewController:self.pageController];
//		[[selfview] addSubview:[self.pageControllerview]];
//		[self.pageControllerdidMoveToParentViewController:self];
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
	
	
	func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
		var viewIndex = viewControllers.indexOfObject(viewController) - 1
		if viewIndex < 0 {
			return nil
		}
		else {
			return viewControllers[viewIndex] as? UIViewController
		}
	}
	
	func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
		var viewIndex = viewControllers.indexOfObject(viewController) + 1
		if viewIndex >= viewControllers.count {
			return nil
		}
		else {
			return viewControllers[viewIndex] as? UIViewController
		}
	}
	
	func viewControllerAtIndex(_index: Int) -> UIViewController? {
		return viewControllers[_index] as? UIViewController
	}
	
	func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
		return viewControllers.count
	}
	
	func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
		return currentIndex
	}
	
}


